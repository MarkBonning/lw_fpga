-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_valve_drive.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning  
--                     Date  :  21/02/2019
--
-- -------------------------------------------------------------------------------
-- description
-- The axle X drive enable will be disabled if the timer has tripped from the WSP 
-- timer in the EM card FPGA(signal is high if tripped) and if either of the 
-- emergency signals are in emergency (signal is low if in emergrency) and hold signal 
-- is Axle X is set or low bsr  signal is enable.
--
-- LSBR_SET is set by being in emergency and by either of low bsr (int or ext) trips 
-- are set and by the LBSR_SET_CONSTANT being enabled in the constants file.
--
-- -------------------------------------------------------------------------------
-- revision 00

-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use work.pvuconstants.all;

entity axle_x_valve_drive is
	port 	(
			clock20mhz_i             			: in std_logic;
			reset_ni  								: in std_logic;

			-- emnergency input
			emergency_i      						: in std_logic;

			-- low bsr 
			lbsr_i									: in std_logic;

			-- timer trip
			axlex_timer_trip_i					: in std_logic;

			sb_axlex_hold_i						: in std_logic;

			wsp_axlex_hold_i						: in std_logic;

			axlex_valve_drive_enable_mon_i	: in std_logic;

			bcpx_i									: in std_logic_vector(11 downto 0);							

			axlex_valve_drive_enable_o 		: out std_logic;

			axlex_valve_drive_error_o 			: out std_logic;
			
			pulse_1ms_i								: in std_logic;
			
			safety_interlock_24v_mon_lw_fpga : in std_logic

			
			
		);
end;

architecture RTL of axle_x_valve_drive is

---- Constants -------------------------------------------------------------------------------
	constant limit_30ms_c					: integer := 30;				-- limit for 30ms counter
	constant limit_100ms_c					: integer := 100;				-- limit for 100ms counter
	constant BCP_LOW							: unsigned(11 downto 0) := x"1d0";
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	signal axlex_valve_drive_enable_s 	: std_logic;
	
	signal bcpx_low_s							: std_logic;

	signal axlex_100ms_counter_s			: integer range 0 to limit_100ms_c := 0;					-- 100ms counter

	signal axlex_30ms_counter_s			: integer range 0 to limit_30ms_c := 0;					-- 30ms counter
	

	
	
----------------------------------------------------------------------------------------------
	
	

begin

	axlex_valve_drive_enable_o <= axlex_valve_drive_enable_s;
	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Axle x enable signal control
-------------------------------------------------------------------------------------------------------------------------------------------------------------------


	axlex_enable_set : process (clock20mhz_i, reset_ni)
	begin
	
		-- reset condition(s)
		if reset_ni = '0' then

			axlex_valve_drive_enable_s <= '0';													
		
		elsif	rising_edge(clock20mhz_i) then

			
			axlex_valve_drive_enable_s <= (not bcpx_low_s) and ( ( (not axlex_timer_trip_i) and (not lbsr_i) and (not wsp_axlex_hold_i )) or ( (not emergency_i) and (not sb_axlex_hold_i) )) ;
		
	
		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Axle x valve drive enable monitor 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	axle_monitor : process (clock20mhz_i, reset_ni)
	begin
		if reset_ni = '0' then

			axlex_valve_drive_error_o <= '0';
			axlex_30ms_counter_s <= 0;
		
		elsif	rising_edge(clock20mhz_i) then
		

			-- check for error when 24V mon is enabled (high)
			if safety_interlock_24v_mon_lw_fpga = '1' then

				-- check enable and monitor are the same value
				-- if no, start the timer
				if axlex_valve_drive_enable_s = axlex_valve_drive_enable_mon_i then
				
					axlex_30ms_counter_s <= 0;
					axlex_valve_drive_error_o <= '0';
				
				
				-- not the same so run the delay
				else

					-- delay over, so flag error
					if axlex_30ms_counter_s = limit_30ms_c then					

						axlex_valve_drive_error_o <= '1';
							
					-- increment count on 1ms pulse if less than 30ms
					elsif pulse_1ms_i = '1' then
						
						axlex_30ms_counter_s <= axlex_30ms_counter_s + 1;
							
					end if;
				
				end if;
		
		
			-- no error when 24V mon is disabled
			else
			
				axlex_30ms_counter_s <= 0;
				axlex_valve_drive_error_o <= '0';
				
			end if;
	
	
	
		end if;
	end process;
		
		
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BCP monitor 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
	bcp_monitor : process (clock20mhz_i, reset_ni)
	begin
		if reset_ni = '0' then

			axlex_100ms_counter_s <= 0;
			bcpx_low_s <= '0';																				
			
			
		elsif	rising_edge(clock20mhz_i) then

			-- if the current BCP pressure is less than the low pressure value
--			if unsigned (bcpx_i) < BCP_LOW then																	

				-- pressure low for 100ms so set the timeout flag
--				if axlex_100ms_counter_s = limit_100ms_c then													
					
					-- timeout active
--					bcpx_low_s <= '1';																				

				-- increment count on 1ms pulse if less than 100ms
--				elsif pulse_1ms_i = '1' then
				
--					axlex_100ms_counter_s <= axlex_100ms_counter_s + 1;
					
					-- timeout not active
--					bcpx_low_s <= '0';																				
--				end if;

			-- pressure ok, so clear counter and timeout flag
--			else

				-- clear the 100ms counter
--				axlex_100ms_counter_s <= 0;														

				-- timeout not active
				bcpx_low_s <= '0';
--			end if;


		end if;
	end process;
		
		


end architecture RTL;