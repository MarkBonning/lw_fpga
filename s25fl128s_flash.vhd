-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  s25fl128s_flash.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  25/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
--
-- Controls an SPI master to read the configuration data from the s25fl128s flash 
-- device.
-- The process reads 16 bits at a time with the required memory address supplied
-- to the module.
--
-- memory_address_i is the memory address ot be read
-- Set read_memory_i high to start the process
-- write_o gets set on completion
-- rx_data_o holds the data read from the device
--
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--USE ieee.std_logic_arith.all;


entity s25fl128s_flash is
    port 	(

      clock20mhz_i      :  in std_logic;
      reset_ni          :  in std_logic;
        
      enable_o				: out std_logic;
      cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		addr_o				: out std_logic_vector(7 downto 0);
		tx_data_o			: out std_logic_vector(47 downto 0);
		busy_i				: in std_logic;
		rx_data_i			: in std_logic_vector(47 downto 0);
		rx_data_o			: out std_logic_vector(15 downto 0);
		write_o				: out std_logic;
		memory_address_i  : in std_logic_vector(23 downto 0);
		read_memory_i		: in std_logic
		

        );
end;

architecture RTL of s25fl128s_flash is

---- Constants -------------------------------------------------------------------------------

	constant read_command_c				: std_logic_vector(7 downto 0) := x"03";

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

type states is ( DELAY, START, RUN, BUSY, DONE, NEXT_READ);
signal state_machine_s   	: states;
----------------------------------------------------------------------------------------------

	
begin

	-- drive constant pins
	cpol_o <= '0';
	cpha_o <= '0';
	addr_o <= (others => '0');
	cont_o <= '0';

	
----------------------------------------------------------------------------------------------
-- Process to read 16 bits of data from the flash memory when requested
----------------------------------------------------------------------------------------------

    s25fl128s_flash_read : 	process ( reset_ni, clock20mhz_i ) is
    
    begin
        if ( reset_ni = '0' ) then
            enable_o <= '0';
				rx_data_o <= (others => '0');
				write_o <= '0';
            state_machine_s <= DELAY;

        elsif Rising_Edge ( clock20mhz_i ) then
        

					-- state machine to run the transfer to the SPI module
					case state_machine_s is
		        		when DELAY =>										
							-- wait for a request to read the data
							if( read_memory_i = '1') then
								state_machine_s <= START;
							else
								state_machine_s <= DELAY;
							end if;
							
						-- setup data and init transfer
						when START =>			
						
							-- start spi transfer
							enable_o <= '1';
							
							-- data is the read command, followed by 3 addres bytes and 2 blank byte to receive
							tx_data_o <= read_command_c & memory_address_i & x"0000";
							
							state_machine_s <= RUN;
							
						-- wait for busy to be high
						when RUN =>											
							enable_o <= '0';
							if busy_i = '0' then
								state_machine_s <= RUN;
							else
								state_machine_s <= BUSY;
							end if;
							
						-- wait for busy to be low
						when BUSY =>										
							if busy_i = '0' then
								state_machine_s <= DONE;
							else
								state_machine_s <= BUSY;
							end if;
							
						-- transfer finished
						when DONE =>		
						
							-- indicate data is ready (16 bits)
							write_o <= '1';					
		
							-- copy data to output
							rx_data_o <= rx_data_i(15 downto 0);
							
							state_machine_s <= NEXT_READ;
							
						when NEXT_READ =>
						
							-- clear write flag
							write_o <= '0';
							
							-- not allowed to start again until read memory goes low
							if( read_memory_i = '0') then					
								state_machine_s <= DELAY;
							else
								state_machine_s <= NEXT_READ;
							end if;
							

		        		when others =>
							state_machine_s <= DELAY;
					end case;		          
					
        end if;
    end process;

end architecture RTL;