-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  tacho_pulses_to_frequency.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  14/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- This module converts the tacho count ot a frequency
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity tacho_pulses_to_frequency is
	port 	(
	
		clock20mhz_i		: in std_logic;		
		reset_ni				: in std_logic;		
		
		-- current pulse count output
		tacho_count_i		: in std_logic_vector(31 downto 0);		

		tacho_frequency_o	: out std_logic_vector(15 downto 0)		
		
		);
end; 

architecture RTL of tacho_pulses_to_frequency is


---- Constants -------------------------------------------------------------------------------

constant counts_per_second_c	: unsigned(31 downto 0) := x"01312d00";	-- 20,000,000 (sys clock)

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

signal tacho_frequency_s	: std_logic_vector(31 downto 0);


-- signals




begin

	

----------------------------------------------------------------------------------------------
-- Frequency count logic
----------------------------------------------------------------------------------------------

	frequency : process (reset_ni, clock20mhz_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			tacho_frequency_o			<= (others => '0');
			tacho_frequency_s <= (others => '0');

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20mhz_i) then	 
		
			if tacho_count_i = x"000000" then 
			
				tacho_frequency_s <= (others => '0');
			
			else
			
				tacho_frequency_s <= std_logic_vector(counts_per_second_c / unsigned(tacho_count_i));
				
			end if;

			tacho_frequency_o <= tacho_frequency_s(15 downto 0);
			
			
	 	end if;
	end process;
	
end architecture RTL;