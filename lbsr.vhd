-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  lbsr.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  14/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register lbsr inputs and lbsr signal
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity lbsr is
	port 	(
	
		clock20mhz_i							: in std_logic;		
		reset_ni									: in std_logic;		
		
		-- low bsr trips
		external_lbsr_trip_lw_fpga_els_i	: in std_logic;
		internal_lbsr_trip_lw_fpga_i		: in std_logic;
		
		-- low bsr enables
		external_lbsr_enable_i				: in std_logic;
		internal_lbsr_enable_i				: in std_logic;

	
		lbsr_o									: out std_logic
		
		
		);
end; 

architecture RTL of lbsr is


---- Constants -------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

signal external_lbsr_trip_lw_fpga_els_s		: std_logic;
signal internal_lbsr_trip_lw_fpga_s				: std_logic;

----------------------------------------------------------------------------------------------


begin

	

----------------------------------------------------------------------------------------------
-- register emergency
----------------------------------------------------------------------------------------------

	lbsr_process : process (reset_ni, clock20mhz_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			external_lbsr_trip_lw_fpga_els_s 	<= '0';
			internal_lbsr_trip_lw_fpga_s			<= '0';
			
			
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20mhz_i) then	 
		
			
			-- register external lbsr 
			if external_lbsr_enable_i = '1' then
			
				external_lbsr_trip_lw_fpga_els_s <= external_lbsr_trip_lw_fpga_els_i;
			
			else
			
				external_lbsr_trip_lw_fpga_els_s <= '0';
			
			end if;
			

			-- register internal lbsr 
			if internal_lbsr_enable_i = '1' then
				
				internal_lbsr_trip_lw_fpga_s <= internal_lbsr_trip_lw_fpga_i;

				
				-- for overall lbsr, internal lbsr take prioroty over external
				lbsr_o <= internal_lbsr_trip_lw_fpga_s;
				
			else

				internal_lbsr_trip_lw_fpga_s <= '0';
				
				-- if internal lbsr not enabled use external
				lbsr_o <= external_lbsr_trip_lw_fpga_els_s;

				
			end if;

		
			
	 	end if;
	end process;
	
end architecture RTL;