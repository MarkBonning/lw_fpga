-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  pvuconstants.vhd
--            Function List  :  None
--                   Author  :  LEESA KINGMAN
--                     Date  :  13/01/2016
--
-- -------------------------------------------------------------------------------
-- description
-- load weight FPGA
-- -------------------------------------------------------------------------------
-- revision 00
--
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
package pvuconstants is

	-- deadband values
--	constant VLCP_DEADBAND_PLUS_MAX 			: unsigned(11 downto 0) := "000000000110";
--	constant VLCP_DEADBAND_MINUS_MIN 		: unsigned(11 downto 0) := "000000000110";
--	constant VLCP_DEADBAND_PLUS_UPPER 		: unsigned(11 downto 0) := "000000000110";
--	constant VLCP_DEADBAND_MINUS_LOWER 		: unsigned(11 downto 0) := "000000000111";
--	constant VLCP_DEADBAND_PLUS_MAX_EMER 	: unsigned(11 downto 0) := "000000000001";
--	constant VLCP_DEADBAND_MINUS_MIN_EMER 	: unsigned(11 downto 0) := "000000000001";
--	constant VLCP_DEADBAND_PLUS_UPPER_EMER : unsigned(11 downto 0) := "000000000011";
--	constant VLCP_DEADBAND_MINUS_LOWER_EMER: unsigned(11 downto 0) := "000000000011";

	constant VLCP_DEADBAND_PLUS_MAX 			: unsigned(11 downto 0) := "000000011011";
	constant VLCP_DEADBAND_MINUS_MIN 		: unsigned(11 downto 0) := "000000001110";
	constant VLCP_DEADBAND_PLUS_UPPER 		: unsigned(11 downto 0) := "000000001110";
	constant VLCP_DEADBAND_MINUS_LOWER 		: unsigned(11 downto 0) := "000000000000";
	
	constant VLCP_DEADBAND_PLUS_MAX_EMER 	: unsigned(11 downto 0) := "000000001110";
	constant VLCP_DEADBAND_MINUS_MIN_EMER 	: unsigned(11 downto 0) := "000000001110";
	constant VLCP_DEADBAND_PLUS_UPPER_EMER : unsigned(11 downto 0) := "000000000111";
	constant VLCP_DEADBAND_MINUS_LOWER_EMER: unsigned(11 downto 0) := "000000000111";


	
constant set_transducer_fault_lower_c 		: unsigned(11 downto 0) := x"147";	--327 = 0.4V
constant clear_transducer_fault_lower_c 	: unsigned(11 downto 0) := x"199";	--409 = 0.5V
constant set_transducer_fault_upper_c 		: unsigned(11 downto 0) := x"f09";	--3849 = 4.7V
constant clear_transducer_fault_upper_c 	: unsigned(11 downto 0) := x"eb7";	--3767 = 4.6V

constant transducer_offset_c 				: unsigned(11 downto 0) := x"199";	--409 = 0.5V

	
end pvuconstants;






























































