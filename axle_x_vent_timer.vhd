-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_vent_timer.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  30/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the axle X vent timer of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_vent_timer is
    port (
    	
    	clock20mhz_i						: in std_logic;								-- system clock
    	reset_ni								: in std_logic;									-- system reset	

		axle_x_timer_enable_i			: in std_logic;								-- Internal FPGA signal to Inlet1 and Vent timer blocks
		axle_x_vent_mv_mon_i				: in std_logic;								-- Axle X vent magnet valve drive monitor


		axle_x_vent_timer_trip_o		: out std_logic;								-- Internal FPGA.

		vent_timeout_period_i			: in std_logic_vector(3 downto 0);		-- Microcontroller Application configuration.
		vent_off_period_i					: in std_logic_vector(3 downto 0);		-- Microcontroller Application configuration.
		vent_timer_reset_mode_i				: in std_logic;							-- Microcontroller Application configuration.
		
		pulse_1ms_i							: in std_logic;								-- single clock pulse (20MHz) every 1ms
		
		vent_timer_count_o				: out std_logic_vector(15 downto 0);
		
		vent_timer_running_o				: out std_logic;
		
		vent_hold_off_timer_running_o	: out std_logic;
		
		vent_hold_off_timer_count_o	: out std_logic_vector(15 downto 0)

				  
		  
    );
end entity axle_x_vent_timer;

architecture imp_axle_x_vent_timer of axle_x_vent_timer is

---- Constants -------------------------------------------------------------------------------
	-- set the min max time constants
	constant max_timeout_ms_c				: integer := 10000;
	constant max_timeout_seconds_c		: integer := 10;
	constant min_timeout_seconds_c		: integer := 4;
	
	-- 1 second in ms
	constant one_second_ms_c				: integer := 1000;
	
	constant vent_current_10mA_s			: std_logic_vector(7 downto 0) := x"0a";	-- assuming 1mA resolution
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	signal	vent_timer_count_s				: unsigned(15 downto 0);					-- 1ms resolution counter for the hold timer timeout and hold off periods
																											-- 16 bits allows full scale of the other two counters to be added together
																											-- although will not be needed in practice
	
	signal vent_timeout_period_s			: unsigned(15 downto 0);					
	signal vent_off_period_s				: unsigned(15 downto 0);					
	signal vent_timer_total_period_s		: unsigned(15 downto 0);					
																					
	-- state machine definision and states
	type states is ( VENT_TIMER_RESET, VENT_TIMER_RUN_INCREMENT, VENT_TIMER_RUN_DECREMENT, VENT_TIMER_VENT_OFF );
	signal state_s   	: states;

----------------------------------------------------------------------------------------------
																					
	

begin

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- create axle x vent trip output
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	vent_trip : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
		
			vent_timer_count_s 				<= (others => '0');
			vent_timeout_period_s 			<= (others => '0');
			vent_off_period_s 				<= (others => '0');
			vent_hold_off_timer_count_o 	<= (others => '0');


			vent_timer_running_o 				<= '0';
			vent_hold_off_timer_running_o 	<= '0';
			axle_x_vent_timer_trip_o 			<= '0';
			
			state_s <= VENT_TIMER_RESET;
			
			
		elsif rising_edge (clock20mhz_i) then
		
		
			-- signal to output mapping
			vent_timer_count_o <= std_logic_vector(vent_timer_count_s);
		
		
			-- the timeout period signal comes from the timeout period register.
			-- the minimum period is 4 seconds
			-- the maximum period is 10 seconds
			-- the register value has a resolution of 1s
			-- If less than 4 seconds or greater than 10 seconds, then set to 10 seconds
			if unsigned(vent_timeout_period_i) < min_timeout_seconds_c or unsigned(vent_timeout_period_i) > max_timeout_seconds_c then
				-- set to 10 seconds
				vent_timeout_period_s <= to_unsigned(max_timeout_ms_c,vent_timeout_period_s'length);
			else 
				-- take register value and convert to ms
				vent_timeout_period_s <= to_unsigned(to_integer(unsigned(vent_timeout_period_i) * one_second_ms_c), vent_timeout_period_s'length);
				
			end if;


			-- the vent off period signal comes from the vent off period register. If less than 4 seconds or greater than 10 seconds, then set to 10 seconds
			if unsigned(vent_off_period_i) < min_timeout_seconds_c or unsigned(vent_off_period_i) > max_timeout_seconds_c then
				vent_off_period_s <= to_unsigned(max_timeout_ms_c, vent_off_period_s'length);
			else 
				vent_off_period_s <= to_unsigned(to_integer(unsigned(vent_off_period_i) * one_second_ms_c), vent_off_period_s'length);
			end if;

			-- total period for the timer = timeout period + vent off period
			vent_timer_total_period_s <= vent_timeout_period_s + vent_off_period_s;


			-- state machine for the timer trip function		
			case state_s is
				when VENT_TIMER_RESET =>

					-- trip is reset
					axle_x_vent_timer_trip_o <= '0';
					
					-- flag that we are not running
					vent_timer_running_o <= '0';

					-- flag the vent hold off timer is not running
					vent_hold_off_timer_running_o <= '0';
					
					-- vent timer count to zero
					vent_hold_off_timer_count_o <= (others => '0');

					
					-- clear timer count
					vent_timer_count_s <= (others => '0');
					

					-- set next state
					if axle_x_timer_enable_i = '1' and axle_x_vent_mv_mon_i = '1' then
						state_s <= VENT_TIMER_RUN_INCREMENT;
					else
						state_s <= VENT_TIMER_RESET;
					end if;

				when VENT_TIMER_RUN_INCREMENT =>

					-- trip remains at reset
					axle_x_vent_timer_trip_o <= '0';
					
					-- flag that we are running
					vent_timer_running_o <= '1';
					
					-- flag the vent hold off timer is not running
					vent_hold_off_timer_running_o <= '0';
					
					-- vent timer count to zero
					vent_hold_off_timer_count_o <= (others => '0');
					
				
					-- on each 10ms pulse increment counter
					if pulse_1ms_i = '1' then
						vent_timer_count_s <= vent_timer_count_s + 1;
					end if;

					-- if vent timer reset mode = 1 and axle_x_vent_mv_mon_i = 0, move to reset state
					if vent_timer_reset_mode_i = '1' and axle_x_vent_mv_mon_i = '0' then
						state_s <= VENT_TIMER_RESET;

					-- if vent timer reset mode = 0 and axle_x_vent_mv_mon_i = 0, return to run decrement state
					elsif vent_timer_reset_mode_i = '0' and axle_x_vent_mv_mon_i= '0' then
						state_s <= VENT_TIMER_RUN_DECREMENT;

					-- if vent timer reset mode = 1 and timer enable is reset, return to reset state
					elsif vent_timer_reset_mode_i = '1' and axle_x_timer_enable_i = '0' then
						state_s <= VENT_TIMER_RESET;

					-- if vent timer reset mode = 0 and timer enable is reset, move to run decrement state
					elsif vent_timer_reset_mode_i = '0' and axle_x_timer_enable_i = '0' then
						state_s <= VENT_TIMER_RUN_DECREMENT;

					-- if the timeout period has been met, move to the vent off state
					elsif vent_timer_count_s = vent_timeout_period_s then
						state_s <= VENT_TIMER_VENT_OFF;
					
					-- else stay in current state						
					else
  						state_s <= VENT_TIMER_RUN_INCREMENT;
					end if;

				when VENT_TIMER_RUN_DECREMENT =>
				
					-- trip remains at reset
					axle_x_vent_timer_trip_o <= '0';
					
					-- flag that we are running
					vent_timer_running_o <= '1';
					
					-- flag the vent hold off timer is not running
					vent_hold_off_timer_running_o <= '0';
					
					-- vent timer count to zero
					vent_hold_off_timer_count_o <= (others => '0');
					
					
					-- if counter greater than zero, on each 10ms pulse decrement counter
					if vent_timer_count_s > 0 and pulse_1ms_i = '1' then
						vent_timer_count_s <= vent_timer_count_s - 1;
					end if;

					-- if vent timer gets back to zero, then return to reset state
					if vent_timer_count_s = 0 then
						state_s <= VENT_TIMER_RESET;
					-- else stay in current state
					else
  						state_s <= VENT_TIMER_RUN_DECREMENT;
					end if;
				

				when VENT_TIMER_VENT_OFF =>

					-- trip is set
					axle_x_vent_timer_trip_o <= '1';

					-- flag that we are not running
					vent_timer_running_o <= '0';
					
					
					-- flag the vent hold off timer is running
					vent_hold_off_timer_running_o <= '1';

					
					-- output the current count
					vent_hold_off_timer_count_o <= std_logic_vector(vent_timer_count_s - unsigned(vent_off_period_i));					
					
					-- on each 10ms pulse increment counter
					if pulse_1ms_i = '1' then
						vent_timer_count_s <= vent_timer_count_s + 1;
					end if;
					
					-- if the total period (timeout + vent off) has been met, return to reset state
					if vent_timer_count_s = vent_timer_total_period_s then
						state_s <= VENT_TIMER_RESET;
					
					-- else remain in current state
					else
						state_s <= VENT_TIMER_VENT_OFF;
					end if;

				when others =>
			end case;
				

		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_vent_timer;

