-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  emergency_in.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  14/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register emergency inputs and set single output if in emergency
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity emergency_in is
	port 	(
	
		clock20mhz_i		: in std_logic;		
		reset_ni				: in std_logic;		
		
		emergency_1_i		: in std_logic;
		emergency_2_i		: in std_logic;

		emergency_o			: out std_logic
		
		
		);
end; 

architecture RTL of emergency_in is


---- Constants -------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

signal emergency_1_s		: std_logic;
signal emergency_2_s		: std_logic;

----------------------------------------------------------------------------------------------


begin

	

----------------------------------------------------------------------------------------------
-- register emergency
----------------------------------------------------------------------------------------------

	emergency : process (reset_ni, clock20mhz_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			emergency_o 	<= '0';

			emergency_1_s 	<= '0';
			emergency_2_s 	<= '0';
			
			
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20mhz_i) then	 
		
			emergency_1_s <= emergency_1_i;
			emergency_2_s <= emergency_2_i;

			-- emergency condition is either emergency signal high
			emergency_o <= emergency_1_s or emergency_2_s;
			
		
			
	 	end if;
	end process;
	
end architecture RTL;