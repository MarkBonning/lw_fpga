-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  eb_code_sim.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  03/04/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Sets EB codes for h/w testing
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity eb_code_sim is
    port 	(

		eb_code_o 					: out std_logic_vector(5 downto 0);
		eb_parity_o 				: out std_logic;
		lw_default_test_o 		: out std_logic;
		register0_O					: out std_logic_vector(15 downto 0);
		register1_O					: out std_logic_vector(15 downto 0);
		register2_O					: out std_logic_vector(15 downto 0);
		register3_O					: out std_logic_vector(15 downto 0);
		
		gain							: in std_logic_vector(11 downto 0);
		eb_config_error			: in std_logic;
		max_vlcp_enable			: in std_logic;
		min_vlcp_enable			: in std_logic;
		ceb_enable					: in std_logic;
		offset						: in std_logic_vector(11 downto 0);
		vlcp_calculated			: in std_logic_vector(11 downto 0);
		aux3							: in std_logic_vector(11 downto 0)

        );
end;

architecture RTL of eb_code_sim is


	
begin


	-- map signals to ports

	eb_code_o				<= "000000";
	eb_parity_o				<= '1';
	lw_default_test_o		<= '0';


	register0_o <= eb_config_error & max_vlcp_enable & min_vlcp_enable & ceb_enable & gain(11 downto 0);
	register1_O <= "0000" & offset;
	register2_O <= "0000" & vlcp_calculated;
	register3_O <= "0000" & aux3;
	

end architecture RTL;