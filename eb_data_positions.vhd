-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  eb_data_constants.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning
--                     Date  :  13/01/2016
--
-- -------------------------------------------------------------------------------
-- description
-- This file defines the memory position constants for the eb data stored in the flash memory
-- -------------------------------------------------------------------------------
-- revision 00
--
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
package eb_data_positions is

constant EB_ID_POSITION										: integer 		:= 16#00#;
constant RELATIONSHIP_BASE_POSITION						: integer 		:= 16#01#;
constant SPEED_SWITCH0_RISING_THRESHOLD_POSITION	: integer 		:= 16#29#;
constant SPEED_SWITCH0_FALLING_THRESHOLD_POSITION	: integer 		:= 16#2a#;
constant SPEED_SWITCH1_RISING_THRESHOLD_POSITION	: integer 		:= 16#2b#;
constant SPEED_SWITCH1_FALLING_THRESHOLD_POSITION	: integer 		:= 16#2c#;
constant SPEED_SWITCH2_RISING_THRESHOLD_POSITION	: integer 		:= 16#2d#;
constant SPEED_SWITCH2_FALLING_THRESHOLD_POSITION	: integer 		:= 16#2e#;
constant LOW_ASP_RISING_THRESHOLD_POSITION			: integer 		:= 16#2f#;
constant LOW_ASP_FALLING_THRESHOLD_POSITION			: integer 		:= 16#30#;
constant ROW_CRC_POSITION									: integer 		:= 16#31#;

constant RELATIONSHIP_GAIN_OFFSET						: integer 		:= 1;
constant RELATIONSHIP_OFFSET_OFFSET						: integer 		:= 2;
constant RELATIONSHIP_MIN_VLCP_OFFSET					: integer 		:= 3;
constant RELATIONSHIP_MAX_VLCP_OFFSET					: integer 		:= 4;
constant RELATIONSHIP_DEFAULT_VLCP_OFFSET				: integer 		:= 5;

constant RELATIONSHIP_LENGTH								: integer 		:= 5;


constant GLOBAL_ENABLES_POSITION							: integer 		:= 16#320#;
constant GLOBAL_PROJECT_IDENTIFIER_POSITION			: integer 		:= 16#321#;
constant GLOBAL_DATA_VERION_POSITION					: integer 		:= 16#322#;
constant GLOBAL_DATASET_FORMAT_VERSION_POSITION		: integer 		:= 16#323#;
constant GLOBAL_CRC_POSITION								: integer 		:= 16#329#;

constant OVERALL_CRC_POSITION								: integer 		:= 16#352#;

constant IMAGE2_OFFSET										: integer 		:= 16#800#;

constant EB_CODE_LENGTH										: integer		:= 50; 		-- including CRC

constant NUMBER_OF_EB_CODES								: integer		:= 16;

constant GLOBAL_DATA_LENGTH								: integer		:= 10; 		-- including CRC

constant CONFIG_LENGTH_BYTES								: integer 		:= (((NUMBER_OF_EB_CODES * EB_CODE_LENGTH) + GLOBAL_DATA_LENGTH) * 2) + 4;  -- including CRC         16#6a4#;


end eb_data_positions;






























































