-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  code_check.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning
--                     Date  :  05/02/2016
--
-- -------------------------------------------------------------------------------
-- description
-- code check module, based on design by LEESA KINGMAN
-- -------------------------------------------------------------------------------
-- revision 0.01: Mark Bonning
--
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use work.lw_relationship_constants.all;

entity code_check is
    port 	(
        clock20mhz_i        	: in  std_logic;
        reset_ni             	: in  std_logic;
        parity_fpga_i        	: in  std_logic;
        lw_default_test_i    	: in  std_logic;
        code_fpga_i          	: in  std_logic_vector( 5 downto 0);
        code_read_fpga_o     	: out std_logic;
        parity_fault_o   		: out std_logic;

		  code_fpga_latched_o  	: out std_logic_vector(5 downto 0);
		  
		  eb_code_valid_o			: out std_logic;
		  
		  parity_latched_o		: out std_logic;
		  
		  pulse_1ms_i				: in std_logic;
		  
		  eb_id_changed_o			: out std_logic						-- flags if the eb id code has changed since being latched
		  

        );
end;

architecture RTL of code_check is

---- Constants -------------------------------------------------------------------------------
    constant counter_limit_c	: integer := 2000;
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

    signal parity_fault_s: std_logic;
	 
    -- 2 second delay count from 1ms pulse
    signal counter_s			: integer range 0 to counter_limit_c := 0;

    signal code_fpga_latched_s 			: std_logic_Vector(5 downto 0);

    signal lw_default_test_le_s        : std_logic;
    signal lw_default_test_del_s       : std_logic;

    type state_code_latch is ( 	START, READY, LATCH, LATCH_RELATIONSHIP_VALUES,
											WAIT_PARITY, STOP    );
    signal state_s     						:	state_code_latch;

----------------------------------------------------------------------------------------------
	 
		

begin

	-- map signals to ports
	code_fpga_latched_o 				<= code_fpga_latched_s;


----------------------------------------------------------------------------------------------
   -- leading edge detection on load weight test signal. So when a rising edge is detected a latch on the code card values can occur
   -- this ensure the latch will only occur once as length of load weight test signal is unknown
   receive_data :process (reset_ni, clock20mhz_i) is
   begin
       if reset_ni = '0' then
           lw_default_test_del_s <=  '0' ;
       elsif rising_edge ( clock20mhz_i ) then
           lw_default_test_del_s <= lw_default_test_i;
       end if;
   end process;
   lw_default_test_le_s <= (not lw_default_test_del_s) and lw_default_test_i;
----------------------------------------------------------------------------------------------
	
	

   -- sets the output signal for parity fault 
   parity_fault_o <= parity_fault_s;
    
	parity_fault_s <= code_fpga_i(0) XOR code_fpga_i(1) XOR code_fpga_i(2) XOR code_fpga_i(3)XOR code_fpga_i(4) XOR code_fpga_i(5) XOR not parity_fpga_i;

	 
	-- flag if the eb id code is different from the latched eb id code
	eb_id_changed_o <= '0' when (code_fpga_latched_s xor code_fpga_i) = "000000" else '1';
	 
	 
    --state machine for controlling the latching of the code signals.
    code_value_read_sm : 	process ( reset_ni, clock20mhz_i ) is
    begin
        if ( reset_ni = '0' ) then
            state_s      			<= START;
            counter_s				<= 0 ;
            code_fpga_latched_s 	<= ( others => '1' ) ;
            code_read_fpga_o 		<= '1';
            eb_code_valid_o 		<= '0';
				parity_latched_o		<= '1';
				
        elsif Rising_Edge ( clock20mhz_i ) then


            case state_s is
               when START =>
						-- set the code read signal, this powers code_fpga and code_parity signals to the FPGA					
						code_read_fpga_o 		<= '1';
						
						-- clear the 2 second counter
                  counter_s <= 0 ;
						
						-- clear the code valid flag as currently not valid
						eb_code_valid_o <= '0';
							
						-- wait for no parity fault to move to next state
						if parity_fault_s = '0' then
							state_s <= READY;
						else
							state_s <= START;
						end if;
						

						when READY =>
						-- if counter has reached lint, then no parity error for 2 seconds, go to next state
						if counter_s = counter_limit_c then 															
							state_s      			<= LATCH;

						-- increment 2 seconds counter
                  elsif parity_fault_s = '0' and pulse_1ms_i = '1' then 								
							counter_s <= counter_s + 1 ;
							state_s      			<= READY;
							
						-- else there is a parity fault
						-- set the default value							
                  elsif parity_fault_s = '1' then															
                     counter_s 				<= 0;
							code_fpga_latched_s 	<= ( others => '1' ) ;
                     state_s      			<= WAIT_PARITY;
						end if;
						
					-- wait for parity value to become valid or for lw_default_test to be true
					when WAIT_PARITY => 																				

							if lw_default_test_le_s = '1' or parity_fault_s = '0' then
                        state_s      <= START;
							else
								state_s      <= WAIT_PARITY;
							end if;

               -- all ok, so latch the eb code 
					when LATCH =>    																					
						parity_latched_o <= parity_fpga_i;
						code_fpga_latched_s 	<= code_fpga_i ;
						eb_code_valid_o <= '1';

                  code_read_fpga_o	<= '0';

						state_s <= STOP;


               -- wait until a reload is required
					-- ********** what if parity becomes invalid while here????
					when STOP => 																				

						if lw_default_test_le_s = '1' then
							state_s  		<= START;
						else
							state_s  <= STOP;
						end if;
						

               when others =>
						state_s <= START;
            end case;
        end if;

    end process;



end architecture RTL;