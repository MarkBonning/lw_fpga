-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  transducer_control.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  03/11/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- COntrols and monitors the pressure transducers
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.pvuconstants.all;


entity transducer_control is
    port (
		clock20mhz_i						: in  std_logic;
      reset_ni								: in  std_logic;
		
		vlcp_i								: in std_logic_vector(11 downto 0);								-- current vlcp input from adc
		aux3_i								: in std_logic_vector(11 downto 0);								-- current aux inputs from adc's
		aux4_i								: in std_logic_vector(11 downto 0);
		aux1_i								: in std_logic_vector(11 downto 0);
		aux2_i								: in std_logic_vector(11 downto 0);
		
		asp1_transducer_source			: in std_logic_vector(2 downto 0);								-- required asp source 1
		asp2_transducer_source			: in std_logic_vector(2 downto 0);								-- required asp source 2

		load_cell1_i						: in std_logic_vector(11 downto 0);								-- load cell 1 value from EX card
		load_cell2_i						: in std_logic_vector(11 downto 0);								-- load cell 1 value from EX card
		
		low_asp_1_o							: out std_logic;
		low_asp_2_o							: out std_logic;
		asp_average_o						: out std_logic_vector(11 downto 0);
		
		vlcp_transducer_fault_o			: out std_logic;
		asp1_transducer_fault_o			: out std_logic;
		asp2_transducer_fault_o			: out std_logic;
		
		pulse_1ms_i							: in std_logic;
		
		low_asp_rising_threshold_i		: in std_logic_vector(11 downto 0);
		low_asp_falling_threshold_i	: in std_logic_vector(11 downto 0)
		
		
		  
    );
end entity transducer_control;

architecture imp_transducer_control of transducer_control is


---- Constants -------------------------------------------------------------------------------
	constant counter_limit_100ms_c 				: integer := 100;									-- Counter limit to give a 100ms delay for the asp averaging
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	signal asp_1_s									: std_logic_vector(11 downto 0);				-- asp is selected aux or load cell input(s)
	signal asp_2_s									: std_logic_vector(11 downto 0);				-- asp is selected aux or load cell input(s)
	signal asp_average_s							: unsigned(11 downto 0);						-- averaged asp from the two averaged inputs
		
	signal asp_1_rolling_average_s			: unsigned(16 downto 0);						-- rolling average over 32 samples
	signal asp_2_rolling_average_s			: unsigned(16 downto 0);						-- rolling average over 32 samples

	signal asp_1_averaged_s						: unsigned(11 downto 0);						-- averaged result
	signal asp_2_averaged_s						: unsigned(11 downto 0);						-- averaged result


	signal counter_100ms_s 						: integer range 0 to counter_limit_100ms_c := 0;				-- counter to hold the current 100ms delay count
	signal pulse_100ms_s							: std_logic;
	
	signal asp_1_used_s 							: std_logic;
	signal asp_2_used_s 							: std_logic;

----------------------------------------------------------------------------------------------
		

begin

-- map signals to ports

asp_average_o 	<= std_logic_vector(asp_average_s);


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- This process gives a single clock pulse every 100ms
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

pulse_100ms:process (clock20mhz_i, reset_ni)
    begin
        -- set initial conditions under reset
		if reset_ni = '0' then

			counter_100ms_s	<= 0;
			pulse_100ms_s <= '0';
				
		elsif rising_edge(clock20mhz_i) then

			
				-- set pulse_100_s on every 100ms
				if counter_100ms_s = counter_limit_100ms_c then
				
					counter_100ms_s	<= 0;
					pulse_100ms_s <= '1';
				
				elsif pulse_1ms_i = '1' then
				
					counter_100ms_s <= counter_100ms_s + 1;
					pulse_100ms_s <= '0';
				
				else 
				
					pulse_100ms_s <= '0';

				
				end if;
			
			end if;
			
	end process;
		  
		  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- This process averages the asp signals
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

average_asp:process (clock20mhz_i, reset_ni)

	variable sample_count : integer range 0 to 31;
	
    begin
    
 
        -- set initial conditions under reset
		if reset_ni = '0' then

			asp_1_rolling_average_s	<= (others => '0');
			asp_2_rolling_average_s	<= (others => '0');
			asp_1_averaged_s	<= (others => '0');
			asp_2_averaged_s	<= (others => '0');
			
			sample_count := 0;
				
		elsif rising_edge(clock20mhz_i) then

			if pulse_100ms_s = '1' then
				
				if sample_count < 31 then
			
					asp_1_rolling_average_s <= asp_1_rolling_average_s + unsigned(asp_1_s);
					asp_2_rolling_average_s <= asp_2_rolling_average_s + unsigned(asp_2_s);
					sample_count := sample_count + 1;
					
				else
				
					asp_1_rolling_average_s <= shift_right(unsigned(asp_1_rolling_average_s), 5);
					asp_2_rolling_average_s <= shift_right(unsigned(asp_2_rolling_average_s), 5);
					asp_1_averaged_s 		<= asp_1_rolling_average_s(16 downto 5);
					asp_2_averaged_s 		<= asp_2_rolling_average_s(16 downto 5);
--					asp_1_o					<= "0000" & std_logic_vector(asp_1_rolling_average_s(16 downto 5));
--					asp_2_o					<= "0000" & std_logic_vector(asp_2_rolling_average_s(16 downto 5));
					
					sample_count  := 0;
				end if;
			
			end if;
			
		end if;
	end process;
		  
		  
		  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- This process calculates the taget vlcp usinG the currently selected asp value(s), gain and offset
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

target_vlcp:
    process (clock20mhz_i, reset_ni)
    begin
        -- set initial conditions under reset
        if reset_ni = '0' then

			asp_average_s 			<= (others => '0');
			asp_1_s <= (others => '0');
			asp_2_s <= (others => '0');

			asp_1_used_s <= '0';
			asp_2_used_s <= '0';
			
				
        elsif rising_edge(clock20mhz_i) then
 
				-- select transducer 1 source
				case asp1_transducer_source is																	
					when "000" =>
						asp_1_s <= (others => '0');
					when "001" =>
						asp_1_s <= aux1_i;
					when "010" =>
						asp_1_s <= aux2_i;
					when "011" =>
						asp_1_s <= aux3_i;
					when "100" =>
						asp_1_s <= aux4_i;
					when "101" =>
						asp_1_s <= load_cell1_i;
					when "110" =>
						asp_1_s <= load_cell2_i;
					when "111" =>
						asp_1_s <= (others => '0');  -- invalid
					when others =>
						asp_1_s <= (others => '0');  -- invalid
				end case; 					
					
				-- select transducer 2 source
				case asp2_transducer_source is																	
					when "000" =>
						asp_2_s <= (others => '0');
					when "001" =>
						asp_2_s <= aux1_i;
					when "010" =>
						asp_2_s <= aux2_i;
					when "011" =>
						asp_2_s <= aux3_i;
					when "100" =>
						asp_2_s <= aux4_i;
					when "101" =>
						asp_2_s <= load_cell1_i;
					when "110" =>
						asp_2_s <= load_cell2_i;
					when "111" =>
						asp_2_s <= (others => '0');	-- invalid
					when others =>
						asp_2_s <= (others => '0');	-- invalid
				end case; 

 				
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- if both the asp source values are valid, then both asp values are averaged by adding together and dividing by 2
-- if only the second asp value is valid, then use that value only
-- for all other options use asp 1
-- *** Note if both asp sources are invalid, then curently asp 1 is used. This probably needs to throw an error
				
			
				-- Both asp's used, so average
				-- not checking for value "111" as not valid form tool
				if (asp1_transducer_source /= "000" and asp2_transducer_source /= "000" ) then
		
				
					asp_average_s <= (asp_1_averaged_s + asp_2_averaged_s) / 2;						-- average the two asp inputs
					
					asp_1_used_s <= '1';
					asp_2_used_s <= '1';

				-- asp 1 not used
				-- not checking for value "111" as not valid form tool
				elsif asp2_transducer_source /= "000" then
				
					asp_average_s <= asp_2_averaged_s(11 downto 0);											-- use asp 2

					asp_1_used_s <= '0';
					asp_2_used_s <= '1';

					
				-- asp 2 not used
				else

					asp_average_s <= asp_1_averaged_s(11 downto 0);											-- use asp 1
				
					asp_1_used_s <= '1';
					asp_2_used_s <= '0';

				end if;
			
		end if;
	
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- This process checks for faults on the transducsers
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

transducer_fault: process (clock20mhz_i, reset_ni)
    begin
        -- set initial conditions under reset
       if reset_ni = '0' then
			
			vlcp_transducer_fault_o <= '0';
			asp1_transducer_fault_o <= '0';
			asp2_transducer_fault_o <= '0';
			
				
		elsif rising_edge(clock20mhz_i) then

			-- set vlcp transducer fault
			if unsigned(vlcp_i) < set_transducer_fault_lower_c or unsigned(vlcp_i) > set_transducer_fault_upper_c then
				vlcp_transducer_fault_o <= '1';
				
			-- clear vlcp transducer fault
			elsif unsigned(vlcp_i) > clear_transducer_fault_lower_c and unsigned(vlcp_i) < clear_transducer_fault_upper_c then
				vlcp_transducer_fault_o <= '0';
			end if;
-------------------------------------------------------

			-- set asp1 transducer fault
			if unsigned(asp_1_s) < set_transducer_fault_lower_c or unsigned(asp_1_s) > set_transducer_fault_upper_c then
				asp1_transducer_fault_o <= '1' and asp_1_used_s;
				
			-- clear asp1 transducer fault
			elsif unsigned(asp_1_s) > clear_transducer_fault_lower_c and unsigned(asp_1_s) < clear_transducer_fault_upper_c then
				asp1_transducer_fault_o <= '0';
			end if;
-------------------------------------------------------
			-- set asp2 transducer fault
			if unsigned(asp_2_s) < set_transducer_fault_lower_c or unsigned(asp_2_s) > set_transducer_fault_upper_c then
				asp2_transducer_fault_o <= '1' and asp_2_used_s;
				
			-- clear asp1 transducer fault
			elsif unsigned(asp_2_s) > clear_transducer_fault_lower_c and unsigned(asp_2_s) < clear_transducer_fault_upper_c then
				asp2_transducer_fault_o <= '0';
			end if;
-------------------------------------------------------

	
		end if;
	end process;


-----------------------------------------------------------------------------------------
-- This process checks for low pressure on the transducsers
-----------------------------------------------------------------------------------------

asp_low_proc : process (clock20mhz_i, reset_ni)
    begin
        -- set initial conditions under reset
       if reset_ni = '0' then
			
			low_asp_1_o <= '0';
			low_asp_2_o <= '0';
			
				
		elsif rising_edge(clock20mhz_i) then

-----------------------------------------------------

			-- set asp1 low if used
			if unsigned(asp_1_averaged_s) < unsigned(low_asp_falling_threshold_i) then
				low_asp_1_o <= '1' and asp_1_used_s;
				
			-- clear asp1 low
			elsif unsigned(asp_1_averaged_s) > unsigned(low_asp_rising_threshold_i) then
				low_asp_1_o <= '0';
			end if;

-----------------------------------------------------

			-- set asp2 low if used
			if unsigned(asp_2_averaged_s) < unsigned(low_asp_falling_threshold_i) then
				low_asp_2_o <= '1' and asp_2_used_s;
				
			-- clear asp2 low
			elsif unsigned(asp_2_averaged_s) > unsigned(low_asp_rising_threshold_i) then
				low_asp_2_o <= '0';
			end if;


		end if;
	end process;


    
end architecture imp_transducer_control;

