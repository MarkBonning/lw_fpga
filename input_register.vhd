-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  input_register.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  03/11/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Single block to register all the inputs
-- Currently does not register the SPI interface signals
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity input_register is
    port (
	 
		clock20mhz_i			: in std_logic;
		reset_ni				: in std_logic;
		
		-- inputs
		adc_eoc_i									: in std_logic;
		parity_fpga_i								: in std_logic;
		gp_io6_i									: in std_logic;
		code0_fpga_i								: in std_logic;
		code1_fpga_i								: in std_logic;
		code2_fpga_i								: in std_logic;
		code3_fpga_i								: in std_logic;
		code4_fpga_i								: in std_logic;
		code5_fpga_i								: in std_logic;
		eb_mode_i									: in std_logic;
		axle2_valve_drive_enable_mon_lw_fpga_i		: in std_logic;
		axle1_valve_drive_enable_mon_lw_fpga_i		: in std_logic;
		micro_bcp_in_range_i						: in std_logic;
		safety_interlock_mon_lw_fpga_i				: in std_logic;
		external_lbsr_trip_lw_fpga_els_i			: in std_logic;
		internal_lbsr_trip_lw_fpga_i				: in std_logic;
		crc_error_latched_i							: in std_logic;
		lw_default_test_i							: in std_locic;
		emergency1_i								: in std_logic;
		emergency2_i								: in std_logic;
		axle2_hold_mv_mon_i							: in std_logic;
		axle1_hold_mv_mon_i							: in std_logic;
		axle2_vent_mv_mon_i							: in std_logic;
		axle1_vent_mv_mon_i							: in std_logic;
		wsp_axle2_vent_i							: in std_logic;
		wsp_axle1_vent_i							: in std_logic;
		wsp_axle2_hold_i							: in std_logic;
		wsp_axle1_hold_i							: in std_logic;
		sb_axle2_hold_i								: in std_logic;
		sb_axle1_hold_i								: in std_logic;
		lw_tacho_iv_probe_select_op_i				: in std_logic;
		axle2_valve_drive_enable_op_i				: in std_logic;
		axle1_valve_drive_enable_op_i				: in std_logic;
		tacho_0_lw_fpga_i							: in std_logic;
		tacho_1_lw_fpga_i							: in std_logic;

		-- outputs
		adc_eoc_o									: out std_logic;
		parity_fpga_o								: out std_logic;
		gp_io6_o									: out std_logic;
		code0_fpga_o								: out std_logic;
		code1_fpga_o								: out std_logic;
		code2_fpga_o								: out std_logic;
		code3_fpga_o								: out std_logic;
		code4_fpga_o								: out std_logic;
		code5_fpga_o								: out std_logic;
		eb_mode_o									: out std_logic;
		axle2_valve_drive_enable_mon_lw_fpga_o		: out std_logic;
		axle1_valve_drive_enable_mon_lw_fpga_o		: out std_logic;
		micro_bcp_in_range_o						: out std_logic;
		safety_interlock_mon_lw_fpga_o				: out std_logic;
		external_lbsr_trip_lw_fpga_els_o			: out std_logic;
		internal_lbsr_trip_lw_fpga_o				: out std_logic;
		crc_error_latched_o							: out std_logic;
		lw_default_test_o							: out std_logic;
		emergency1_o								: out std_logic;
		emergency2_o								: out std_logic;
		axle2_hold_mv_mon_o							: out std_logic;
		axle1_hold_mv_mon_o							: out std_logic;
		axle2_vent_mv_mon_o							: out std_logic;
		axle1_vent_mv_mon_o							: out std_logic;
		wsp_axle2_vent_o							: out std_logic;
		wsp_axle1_vent_o							: out std_logic;
		wsp_axle2_hold_o							: out std_logic;
		wsp_axle1_hold_o							: out std_logic;
		sb_axle2_hold_o								: out std_logic;
		sb_axle1_hold_o								: out std_logic;
		lw_tacho_iv_probe_select_op_o				: out std_logic;
		axle2_valve_drive_enable_op_o				: out std_logic;
		axle1_valve_drive_enable_op_o				: out std_logic;
		tacho_0_lw_fpga_o							: out std_logic;
		tacho_1_lw_fpga_o							: out std_logic;


		
		
		
    );
end entity input_register;

architecture RTL of input_register is

-- Signals ---------------------------------------------------------------------------------

signal adc_eoc_s									: std_logic;
signal parity_fpga_s								: std_logic;
signal gp_io6_s										: std_logic;
signal code0_fpga_s									: std_logic;
signal code1_fpga_s									: std_logic;
signal code2_fpga_s									: std_logic;
signal code3_fpga_s									: std_logic;
signal code4_fpga_s									: std_logic;
signal code5_fpga_s									: std_logic;
signal eb_mode_s									: std_logic;
signal axle2_valve_drive_enable_mon_lw_fpga_s		: std_logic;
signal axle1_valve_drive_enable_mon_lw_fpga_s		: std_logic;
signal micro_bcp_in_range_s							: std_logic;
signal safety_interlock_mon_lw_fpga_s				: std_logic;
signal external_lbsr_trip_lw_fpga_els_s				: std_logic;
signal internal_lbsr_trip_lw_fpga_s					: std_logic;
signal crc_error_latched_s							: std_logic;
signal lw_default_test_s							: std_logic;
signal emergency1_s									: std_logic;
signal emergency2_s									: std_logic;
signal axle2_hold_mv_mon_s							: std_logic;
signal axle1_hold_mv_mon_s							: std_logic;
signal axle2_vent_mv_mon_s							: std_logic;
signal axle1_vent_mv_mon_s							: std_logic;
signal wsp_axle2_vent_s								: std_logic;
signal wsp_axle1_vent_s								: std_logic;
signal wsp_axle2_hold_s								: std_logic;
signal wsp_axle1_hold_s								: std_logic;
signal sb_axle2_hold_s								: std_logic;
signal sb_axle1_hold_s								: std_logic;
signal lw_tacho_iv_probe_select_op_s				: std_logic;
signal axle2_valve_drive_enable_op_s				: std_logic;
signal axle1_valve_drive_enable_op_s				: std_logic;
signal tacho_0_lw_fpga_s							: std_logic;
signal tacho_1_lw_fpga_s							: std_logic;


--------------------------------------------------------------------------------------------

begin


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to register the inputs
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	input_reg : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then

			adc_eoc_s <= '0';
			adc_eoc_o <= '0';
			
			parity_fpga_s <= '0';
			parity_fpga_o <= '0';
			
			gp_io6_s <= '0';
			gp_io6_o <= '0';

			code0_fpga_s <= '0';
			code0_fpga_o <= '0';
			
			eb_mode_s <= '0';
			eb_mode_o <= '0';
			
			axle2_valve_drive_enable_mon_lw_fpga_s <= axle2_valve_drive_enable_mon_lw_fpga_i;
			axle2_valve_drive_enable_mon_lw_fpga_o <= axle2_valve_drive_enable_mon_lw_fpga_s;

			axle1_valve_drive_enable_mon_lw_fpga_s <= axle1_valve_drive_enable_mon_lw_fpga_i;
			axle1_valve_drive_enable_mon_lw_fpga_o <= axle1_valve_drive_enable_mon_lw_fpga_s;

			micro_bcp_in_range_s <= '0';
			micro_bcp_in_range_o <= '0';
			
			safety_interlock_mon_lw_fpga_s <= '0';
			safety_interlock_mon_lw_fpga_o <= '0';
			
			external_lbsr_trip_lw_fpga_els_s <= '0';
			external_lbsr_trip_lw_fpga_els_o <= '0';
			
			internal_lbsr_trip_lw_fpga_s <= '0';
			internal_lbsr_trip_lw_fpga_o <= '0';
			
			crc_error_latched_s <= '0';
			crc_error_latched_o <= '0';
			
			lw_default_test_s <= '0';
			lw_default_test_o <= '0';
			
			emergency1_s <= '0';
			emergency1_o <= '0';
			
			emergency2_s <= '0';
			emergency2_o <= '0';
			
			axle2_hold_mv_mon_s <= '0';
			axle2_hold_mv_mon_o <= '0';
			
			axle1_hold_mv_mon_s <= '0';
			axle1_hold_mv_mon_o <= '0';		
			
			axle2_vent_mv_mon_s <= '0';
			axle2_vent_mv_mon_o <= '0';		
			
			axle1_vent_mv_mon_s <= '0';
			axle1_vent_mv_mon_o <= '0';		
			
			wsp_axle2_vent_s <= 0;
			wsp_axle2_vent_o <= 0;			
						
			wsp_axle1_vent_s <= 0;
			wsp_axle1_vent_o <= 0;			
						
			wsp_axle2_hold_s <= 0;
			wsp_axle2_hold_o <= 0;			
						
			wsp_axle1_hold_s <= 0;
			wsp_axle1_hold_o <= 0;	
			
			sb_axle2_hold_s <= '0';
			sb_axle2_hold_o <= '0';

			sb_axle1_hold_s <= '0';
			sb_axle1_hold_o <= '0';
			
			lw_tacho_iv_probe_select_op_s <= '0';
			lw_tacho_iv_probe_select_op_o <= '0';

			axle2_valve_drive_enable_op_s <= '0';
			axle2_valve_drive_enable_op_o <= '0';

			axle1_valve_drive_enable_op_s <= '0';
			axle1_valve_drive_enable_op_o <= '0';

			tacho_0_lw_fpga_s <= '0';
			tacho_0_lw_fpga_o <= '0';

			tacho_1_lw_fpga_s <= '0';
			tacho_1_lw_fpga_o <= '0';
			
						
		elsif rising_edge (clock20mhz_i) then
		
			adc_eoc_s <= adc_eoc_i;
			adc_eoc_o <= adc_eoc_s;
		
			parity_fpga_s <= parity_fpga_i;
			parity_fpga_o <= parity_fpga_i;
		
			gp_io6_s <= gp_io6_i;
			gp_io6_o <= gp_io6_s;
			
			code0_fpga_s <= code0_fpga_i;
			code0_fpga_o <= code0_fpga_s;
			
			code1_fpga_s <= code1_fpga_i;
			code1_fpga_o <= code1_fpga_s;
			
			code2_fpga_s <= code2_fpga_i;
			code2_fpga_o <= code2_fpga_s;
			
			code3_fpga_s <= code3_fpga_i;
			code3_fpga_o <= code3_fpga_s;
			
			code4_fpga_s <= code4_fpga_i;
			code4_fpga_o <= code4_fpga_s;
			
			code5_fpga_s <= code5_fpga_i;
			code5_fpga_o <= code5_fpga_s;
			
			eb_mode_s <= eb_mode_i;
			eb_mode_o <= eb_mode_s;
			
			axle2_valve_drive_enable_mon_lw_fpga_s <= axle2_valve_drive_enable_mon_lw_fpga_i;
			axle2_valve_drive_enable_mon_lw_fpga_o <= axle2_valve_drive_enable_mon_lw_fpga_s;
			
			axle1_valve_drive_enable_mon_lw_fpga_s <= axle1_valve_drive_enable_mon_lw_fpga_i;
			axle1_valve_drive_enable_mon_lw_fpga_o <= axle1_valve_drive_enable_mon_lw_fpga_s;
			
			micro_bcp_in_range_s <= micro_bcp_in_range_i;
			micro_bcp_in_range_o <= micro_bcp_in_range_s;
			
			safety_interlock_mon_lw_fpga_s <= safety_interlock_mon_lw_fpga_i;
			safety_interlock_mon_lw_fpga_o <= safety_interlock_mon_lw_fpga_s;
			
			external_lbsr_trip_lw_fpga_els_s <= external_lbsr_trip_lw_fpga_els_i;
			external_lbsr_trip_lw_fpga_els_o <= external_lbsr_trip_lw_fpga_els_s;
			
			internal_lbsr_trip_lw_fpga_s <= internal_lbsr_trip_lw_fpga_i;
			internal_lbsr_trip_lw_fpga_o <= internal_lbsr_trip_lw_fpga_s;
			
			crc_error_latched_s <= crc_error_latched_i;
			crc_error_latched_o <= crc_error_latched_s;
			
			lw_default_test_s <= lw_default_test_i;
			lw_default_test_o <= lw_default_test_s;
			
			emergency1_s <= emergency1_i;
			emergency1_o <= emergency1_s;
			
			emergency2_s <= emergency2_i;
			emergency2_o <= emergency2_s;
			
			axle2_hold_mv_mon_s <= axle2_hold_mv_mon_i;
			axle2_hold_mv_mon_o <= axle2_hold_mv_mon_s;
			
			axle1_hold_mv_mon_s <= axle1_hold_mv_mon_i;
			axle1_hold_mv_mon_o <= axle1_hold_mv_mon_s;		
			
			axle2_vent_mv_mon_s <= axle2_vent_mv_mon_i;
			axle2_vent_mv_mon_o <= axle2_vent_mv_mon_s;		
			
			axle1_vent_mv_mon_s <= axle1_vent_mv_mon_i;
			axle1_vent_mv_mon_o <= axle1_vent_mv_mon_s;		
			
			wsp_axle2_vent_s <= wsp_axle2_vent_i;
			wsp_axle2_vent_o <= wsp_axle2_vent_s;

			wsp_axle1_vent_s <= wsp_axle1_vent_i;
			wsp_axle1_vent_o <= wsp_axle1_vent_s;

			wsp_axle2_hold_s <= wsp_axle2_hold_i;
			wsp_axle2_hold_o <= wsp_axle2_hold_s;

			wsp_axle1_hold_s <= wsp_axle1_hold_i;
			wsp_axle1_hold_o <= wsp_axle1_hold_s;
			
			sb_axle2_hold_s <= sb_axle2_hold_i;
			sb_axle2_hold_o <= sb_axle2_hold_s;

			sb_axle1_hold_s <= sb_axle1_hold_i;
			sb_axle1_hold_o <= sb_axle1_hold_s;
			
			lw_tacho_iv_probe_select_op_s <= lw_tacho_iv_probe_select_op_i;
			lw_tacho_iv_probe_select_op_o <= lw_tacho_iv_probe_select_op_s;
			
			axle2_valve_drive_enable_op_s <= axle2_valve_drive_enable_op_i;
			axle2_valve_drive_enable_op_o <= axle2_valve_drive_enable_op_s;

			axle1_valve_drive_enable_op_s <= axle1_valve_drive_enable_op_i;
			axle1_valve_drive_enable_op_o <= axle1_valve_drive_enable_op_s;

			tacho_0_lw_fpga_s <= tacho_0_lw_fpga_i;
			tacho_0_lw_fpga_o <= tacho_0_lw_fpga_s;

			tacho_1_lw_fpga_s <= tacho_1_lw_fpga_i;
			tacho_1_lw_fpga_o <= tacho_1_lw_fpga_s;
			
		
		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture RTL;

