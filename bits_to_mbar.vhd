-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  bit_to_mbar.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  03/11/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Converts the 12 bit ADC number to millibar. 
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity bits_to_mbar is
    port (
	 
		clock20mhz_i			: in std_logic;
		reset_ni					: in std_logic;

		bits_i					: in std_logic_vector(11 downto 0);
		millibar_o				: out std_logic_vector(15 downto 0);
		
		transducer_fault_o	: out std_logic
		
		
    );
end entity bits_to_mbar;

architecture imp_bits_to_mbar of bits_to_mbar is

constant microbar_per_bit_c 	: unsigned(11 downto 0) := x"e4f";	--3663 uBar per bit

constant transducer_offset_c 	: unsigned(11 downto 0) := x"199";	--409 = 0.5V

signal	millibar_s				: unsigned(23 downto 0);

-- these are raw bit values as the lower ones do not convert into a positive pressure
constant set_transducer_fault_lower_c 		: unsigned(11 downto 0) := x"147";	--327 = 0.4V
constant clear_transducer_fault_lower_c 	: unsigned(11 downto 0) := x"199";	--409 = 0.5V
constant set_transducer_fault_upper_c 		: unsigned(11 downto 0) := x"f09";	--3849 = 4.7V
constant clear_transducer_fault_upper_c 	: unsigned(11 downto 0) := x"eb7";	--3767 = 4.6V



begin


-- logic

-- ADC value, minus the offset (0.5V) multiplied by the number of microbar per bit (3663) then divided by 1000 to give millibar
millibar_s	<= ((bits_i - transducer_offset_c) * microbar_per_bit_c) / 1000;

-- convert to 16 bit std_logic_vector
millibar_o <= std_logic_vector(millibar_s(15 downto 0));


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- transducer fault
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	transducer_fault : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then

			-- clear fault on reset
			transducer_fault_o <= '0';
			
		elsif rising_edge (clock20mhz_i) then
		
		if bits_i >= transducer_offset_c 
		
		
		end if;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_bits_to_mbar;

