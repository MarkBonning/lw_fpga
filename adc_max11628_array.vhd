-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--                File Name  :  adc_MAX11628.vhd
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- This code provides setup, then continous sample of the ADC
-- It controls an SPI master to interface to the MAX11628 ADC
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity adc_max11628_array is
    port 	(

      clock20mhz_i        :  in std_logic;
      reset_ni            :  in std_logic;
        
     	enable_o				: out std_logic;
      cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		tx_data_o			: out std_logic_vector(7 downto 0);												-- the data to send to the adc
		busy_i				: in std_logic;																		-- busy flag fo rthe read / write operation
		adc_eoc_ni			: in std_logic;																		-- adc end of conversion flag
		rx_data_i			: in std_logic_vector(7 downto 0);												-- data received from the adc
--		averaging_i			: in std_logic_vector(7 downto 0);												-- value to provide internal averaging in the adc (needs agrreeing with Knorr and may get removed)

		aux1_o		: out std_logic_vector(15 downto 0);													-- adc value for aux 1, 12 bits
		aux2_o		: out std_logic_vector(15 downto 0);													-- adc value for aux 2, 12 bits
		aux3_o		: out std_logic_vector(15 downto 0);													-- adc value for aux 3, 12 bits
		aux4_o		: out std_logic_vector(15 downto 0);													-- adc value for aux 4, 12 bits
		bcp1_o		: out std_logic_vector(15 downto 0);													-- adc value for bcp 1, 12 bits
		bcp2_o		: out std_logic_vector(15 downto 0);													-- adc value for bcp 2, 12 bits
		vlcp_o		: out std_logic_vector(15 downto 0);													-- adc value for vlcp, 12 bits
		bsrp_o		: out std_logic_vector(15 downto 0);													-- adc value for bsrp,  12 bits
		
		pulse_1ms_i	: in std_logic
		
        );
end;

architecture RTL of adc_max11628_array is


---- Constants -------------------------------------------------------------------------------

constant convertion_register_val_c	: std_logic_vector(7 downto 0) := x"b8";
constant config_register_val_c		: std_logic_vector(7 downto 0) := x"64";
constant averaging_register_val_c	: std_logic_vector(7 downto 0) := x"20";

constant timeout_val_c					: integer := 1000;   -- 1 second
constant number_of_config_bytes_c	: integer := 3;		-- includes Start conversion
constant number_of_data_bytes_c		: integer := 16;
constant number_of_delay_clocks_c	: integer := 100;

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------
																															-- state machine states
type states is ( RESET, START, SPI_WAIT, BYTE_CHECK, CONVERSION_WAIT, COPY_DATA, DELAY_LOOP );
signal state_s   		: states;

signal adc_data_s		: std_logic_vector(175 downto 0);														-- register for the complete set of adc reads
signal adc_timeout_s	: std_logic;

signal timeout_count : integer range 0 to timeout_val_c := 0;
signal byte_count_s	: integer range 0 to 20:= 0;

signal delay_count_s : integer range 0 to 128:= 0;


----------------------------------------------------------------------------------------------


begin

	-- drive constant pins
	cpol_o <= '1';
	cpha_o <= '1';
	cont_o <= '0';
	
----------------------------------------------------------------------------------------------
-- process to read the adc values
----------------------------------------------------------------------------------------------

adc : process ( reset_ni, clock20mhz_i ) is

		
		-- variables for setup lookup table
		 type MEM is array(0 to number_of_config_bytes_c - 1) of std_logic_vector(7 downto 0);
		 variable adc_setup : MEM;
    
    
    begin
	 
	 -- register setup values and start conversion;
	 adc_setup := ( config_register_val_c, averaging_register_val_c, convertion_register_val_c );
	 
	 
	----------------------------------------------------------------------------------------------
	-- reset loads the default parameters
		 
		if reset_ni = '0' then
			enable_o <= '0';
			byte_count_s <= 0;
			adc_data_s <= (others => '0');
			tx_data_o <= (others => '0');
			timeout_count <= 0;
			adc_timeout_s <= '0';
			delay_count_s <= 0;
			
			state_s <= RESET;
			
		elsif Rising_Edge ( clock20mhz_i ) then
		

			-- create 1 second timeout in case adc's lock up or lose power
			-- initiales a restart in FSM
			if state_s = RESET then
			
				-- clear timeout count
				timeout_count <= 0;

				adc_timeout_s <= '0';
			
			elsif timeout_count = timeout_val_c then
			
				adc_timeout_s <= '1';
				
			elsif pulse_1ms_i = '1' then
			
				adc_timeout_s <= '0';
				timeout_count <= timeout_count + 1;
				
			end if;

        
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- state machine to run the transfer to the SPI module

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Send setup data value - loop through each register
-- currently only 2 setup registers used.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

			case state_s is
				when RESET =>																					
				
					byte_count_s <= 0;
					adc_data_s <= (others => '0');
	
				
					-- start on a 1ms timer
					if pulse_1ms_i = '1' then
						state_s <= START;
					else
						state_s <= RESET;
					end if;
				
				-- load the setup data
				when START =>																					
					enable_o <= '1';
					
					if byte_count_s < 3 then
					
						tx_data_o <= adc_setup(byte_count_s);
						
					else
						
						tx_data_o <= (others => '0');
											
					end if;
					
					if busy_i = '0' then
						state_s <= START;
					else
						state_s <= SPI_WAIT;
					end if;

				-- wait for SPI to finish
				when SPI_WAIT =>																			
					enable_o <= '0';
					delay_count_s <= 0;
					if busy_i = '0' then

						-- copy the data to the buffer
						adc_data_s <= adc_data_s(167 downto 0) & rx_data_i;  							

						-- move to the next byte
						byte_count_s <= byte_count_s + 1;																	
						state_s <= DELAY_LOOP;
					else
						state_s <= SPI_WAIT;
					end if;
					
				-- from datasheet, delay between chip selects appears to be up to 4us
				when DELAY_LOOP =>
				
					if delay_count_s < number_of_delay_clocks_c then
						
						delay_count_s <= delay_count_s + 1;
						state_s <= DELAY_LOOP;

					else
					
						state_s <= BYTE_CHECK;
					end if;					
					
					
				
				-- if more config bytes to do head back to setup, else start conversions
				when BYTE_CHECK =>																			

				
					-- if finished setup and conversion, then wait for conversion to finish
					if byte_count_s = number_of_config_bytes_c then
					
						state_s <= CONVERSION_WAIT;
						
					-- check if the complete transfer cycle has completed
					elsif byte_count_s = number_of_data_bytes_c + number_of_config_bytes_c then
					
						state_s <= COPY_DATA;
					
					-- else, round again for next byte
					else
						state_s <= START;
					end if;


				-- wait  for conversion to finish
				when CONVERSION_WAIT =>																		

					byte_count_s <= 0;

					-- has the conversion finished or timed out
					-- if timeout, we still need to read the bytes, just not use them
					if adc_eoc_ni = '0' or adc_timeout_s = '1' then

						state_s <= START;
					
					else
					
						state_s <= CONVERSION_WAIT;
						
					end if;
					
				when COPY_DATA =>
																							
					-- only load the data if there was not a timeout
					if adc_timeout_s = '0' then

						vlcp_o <= "0000" & adc_data_s(123 downto 112);
						aux3_o <= "0000" & adc_data_s(107 downto 96);
						aux4_o <= "0000" & adc_data_s(91 downto 80);
						aux1_o <= "0000" & adc_data_s(75 downto 64);
						aux2_o <= "0000" & adc_data_s(59 downto 48);
						bsrp_o <= "0000" & adc_data_s(43 downto 32);
						bcp1_o <= "0000" & adc_data_s(27 downto 16);
						bcp2_o <= "0000" & adc_data_s(11 downto 0);
					
					end if;
					
					state_s <= RESET;
					
										
				-- something went wrong, re-setup adc
				when others =>
					state_s <= RESET;
			end case;		          

					
        end if;
    end process;


end architecture RTL;