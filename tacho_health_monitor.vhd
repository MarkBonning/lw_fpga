-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  tacho_health_monitor.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  10/01/2020
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Monitors the tacho signals and decides if they are healthy or not
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.system_constants.all;


entity tacho_health_monitor is
    port (
	 
		clock20mhz_i			: in std_logic;
		reset_ni					: in std_logic;
		
		pulse_1ms				: in std_logic;

		tacho_0_min_speed_detected_i		: in std_logic;
		tacho_0_max_speed_detected_i		: in std_logic;
		tacho_1_min_speed_detected_i		: in std_logic;
		tacho_1_max_speed_detected_i		: in std_logic;
		
		tacho_healthy_o		: out std_logic
		
		
		
    );
end entity tacho_health_monitor;

architecture imp_tacho_health_monitor of tacho_health_monitor is

---- Constants -------------------------------------------------------------------------------
-- limit for fault timer counter (currently 500ms, but will change)
constant limit_fault_timer_c			: integer := 500;				
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------
signal fault_timer_s						: integer range 0 to limit_fault_timer_c := 0;					
----------------------------------------------------------------------------------------------


begin


-- logic



-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- check all the parameters
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	tacho_check : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then

			-- assumes heatlthy on reset
			tacho_healthy_o <= HEALTHY;
			
			-- clear the fault timer
			fault_timer_s <= 0;
			
		
		elsif rising_edge (clock20mhz_i) then
		
			
			-- check all the unhealthy options
			if (tacho_0_min_speed_detected_i = '1' and tacho_1_min_speed_detected_i = '0') or
				(tacho_0_min_speed_detected_i = '0' and tacho_1_min_speed_detected_i = '1') or
				 tacho_0_max_speed_detected_i = '1' or tacho_1_max_speed_detected_i = '1' then
		
					-- if the timer has hit the limit then we are unhealthy
					if fault_timer_s = limit_fault_timer_c then
					
						tacho_healthy_o <= UNHEALTHY;
					
					-- else increase count but keep healthy
					else

						tacho_healthy_o <= HEALTHY;
						
						-- increment count every 1ms
						if pulse_1ms = '1' then
						
							fault_timer_s <= fault_timer_s + 1;
							
						end if;
						
					end if;
				
				-- else no fault
				else
				
					tacho_healthy_o <= HEALTHY;

					-- clear the fault timer
					fault_timer_s <= 0;
					
				end if;
			

			
		
		end if;
		
		end process;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_tacho_health_monitor;

