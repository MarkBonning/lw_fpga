-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  ex_fpga_image_numbers.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  23/08/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Holds the Ex fpga image part number and issue number to connect to the mux. 
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity lw_fpga_image_numbers is
    port (

		reg_o			: out std_logic_vector(15 downto 0)
    );
end entity lw_fpga_image_numbers;

architecture imp_lw_fpga_image_numbers of lw_fpga_image_numbers is

	
constant fpga_image_number 			: std_logic_vector(9 downto 0) := "0100010101";    -- 277
constant fpga_image_issue_number 	: std_logic_vector(5 downto 0) := "000000";

begin


-- logic

reg_o(15 downto 6) 	<= fpga_image_number;
reg_o(5 downto 0) 	<= fpga_image_issue_number;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_lw_fpga_image_numbers;

