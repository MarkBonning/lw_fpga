-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  eb_config_loader.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  06/02/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Loads config data into the code checker, gets data via the flash memory reader
-- Also gets the rolling CRC value and uses the last value to compare against 
-- stored flash memory value.
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.02: Mark Bonning 03/02/2020 -- Updated speed input to use highest of the two tacho values
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.eb_data_positions.all;


entity eb_config_loader is
    port (
      clk_20mhz_i							: in  std_logic;
      reset_ni								: in  std_logic;
      eb_data_i							: in  std_logic_vector (15 downto 0);				-- data from the config flash
      memory_read_o						: out  std_logic;											-- tell the memory controller to read data
		data_ready_i						: in std_logic;											-- data ready signal from config flash driver
		  

      memory_address_o					: out  std_logic_vector(23 downto 0);				-- address to read frmo the flash memory
																								
		  
      gain_o             				: out std_logic_vector(11 downto 0);				-- output for the current gain value
      offset_o           				: out std_logic_vector(15 downto 0);				-- output for the current offset value
      default_vlcp_o						: out std_logic_vector(11 downto 0);				-- output for the current vlcp value
      max_vlcp_o							: out std_logic_vector(11 downto 0);				-- output for the max vlcp value
		min_vlcp_o							: out std_logic_vector(11 downto 0);				-- output for the min vlcp value

		asp_rising_threshold_o			: out std_logic_vector(11 downto 0);
		asp_falling_threshold_o			: out std_logic_vector(11 downto 0);
		  
		eb_code_i							: in std_logic_vector(5 downto 0);					-- the eb code to load
		eb_code_valid_i					: in std_logic;											-- indicates the eb id codeis valid
	  
		crc_clear_o							: out std_logic;											-- reset for the local crc
		crc_data_o							: out std_logic_vector(15 downto 0);				-- data to crc calculator
		crc_load_o							: out std_logic;											-- loads the local crc data into the crc calculator
		crc_data_i							: in std_logic_vector(15 downto 0);					-- result from crc calculator
		  
		tacho_frequency0_i				: in std_logic_vector(15 downto 0);					-- current speed pulses;
		tacho_frequency1_i				: in std_logic_vector(15 downto 0);					-- current speed pulses;
		  
		external_lbsr_trip_lw_fpga_els : in std_logic;
		 
		ceb_enabled_o						: out std_logic;
		internal_lbsr_enabled_o			: out std_logic;
		external_lbsr_enabled_o			: out std_logic;
		tacho_probe_select_o				: out std_logic;
		asp1_transducer_source_o		: out std_logic_vector(2 downto 0);
		asp2_transducer_source_o		: out std_logic_vector(2 downto 0);
			
		  
		lw_fpga_op_enable_o				: out std_logic;
		error_o								: out std_logic;
		  
		overall_data_crc_o				: out std_logic_vector(15 downto 0);
		global_data_crc_o					: out std_logic_vector(15 downto 0);
		eb_id_data_crc_o					: out std_logic_vector(15 downto 0);
		  
		config_write_o						: out std_logic;														-- flag to tell the mux to load the config crc values
		  
		eb_id_crc_error_o					: out std_logic;														-- to the mux
		global_data_crc_error_o			: out std_logic;														-- to the mux
		overall_data_crc_error_o		: out std_logic;														-- to the mux
		  
		data_version_o						: out std_logic_vector(7 downto 0);
		dataset_format_version_o		: out std_logic_vector(7 downto 0);
		project_identifier_o				: out std_logic_vector(31 downto 0);
		  
		ex_digital_inputs_i				: in std_logic_vector(6 downto 0);
		  
		vent_timer_reset_mode_o			: out std_logic;
		eb_id_quantity_o					: out std_logic_vector(3 downto 0);
		  
		ceb_variables_1_o					: out std_logic_vector(15 downto 0);
		ceb_variables_2_o					: out std_logic_vector(15 downto 0);
		ceb_variables_3_o					: out std_logic_vector(15 downto 0);

		hold_off_wsp_timing_o			: out std_logic_vector(3 downto 0);
		hold_timeout_wsp_timing_o		: out std_logic_vector(3 downto 0);
		vent_off_wsp_timing_o			: out std_logic_vector(3 downto 0);
		vent_timeout_wsp_timing_o		: out std_logic_vector(3 downto 0);
		
		tacho_healthy_i					: in std_logic;
		
		eb_id_indication_o				: out std_logic_vector(3 downto 0);
		
		pulse_1ms_i							: in std_logic;
		
		relationship_o						: out std_logic_vector(2 downto 0);
		
		debug0_o					: out std_logic_vector(15 downto 0);
		debug1_o					: out std_logic_vector(15 downto 0);
		debug2_o					: out std_logic_vector(15 downto 0)
		  
		  
    );
end entity eb_config_loader;

architecture imp_eb_config_loader of eb_config_loader is


---- Constants -------------------------------------------------------------------------------

	-- these need the correct values
	constant GAIN_DEFAULT				: std_logic_vector(11 downto 0 ) := x"000";
	constant OFFSET_DEFAULT				: std_logic_vector(15 downto 0 ) := x"0000";
	constant VLCP_DEFAULT				: std_logic_vector(11 downto 0 ) := x"4cd";	 -- 3 bar

	constant valve_configuration_table_length 	: integer := 49;
	constant global_configuration_table_length 	: integer := 9;

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	-- State machine states for loading the config data
	type state_load_config is ( COMPARE_START, COMPARE_REQUEST1, COMPARE_WAIT_FOR_FLASH1, COMPARE_LOAD1, COMPARE_REQUEST2, COMPARE_WAIT_FOR_FLASH2, COMPARE_LOAD2, COMPARE_LOOP,
								LOAD_START, LOAD_OFFSET, LOAD_REQUEST, LOAD_WAIT_FOR_FLASH, LOAD_LOAD, LOAD_CHECK_LENGTH, LOAD_REQUEST_CRC, LOAD_WAIT_FOR_CRC, LOAD_COMPARE_CRC,
								GLOBAL_START, GLOBAL_OFFSET, GLOBAL_REQUEST, GLOBAL_WAIT_FOR_FLASH, GLOBAL_LOAD, GLOBAL_CHECK_LENGTH, GLOBAL_REQUEST_CRC, GLOBAL_WAIT_FOR_CRC, GLOBAL_COMPARE_CRC,
								STOP);
	signal state_s     :	state_load_config;


	signal crc_stored_s : std_logic_vector(15 downto 0);					-- crc from the flash memory

	signal memory_address_s	: std_logic_vector(23 downto 0);				-- flash memory address (can have second image offset applied
	signal memory_count_s	: unsigned(23 downto 0);						-- flash memory address count

	signal compare_data : std_logic_vector(15 downto 0);

	-- selected EB code configuration
	type valve_configuration_table is array (0 to valve_configuration_table_length) of std_logic_vector(15 DOWNTO 0);
	signal valve_configuration_data_s: valve_configuration_table;

	-- global configuration data
	type global_configuration_table is array (0 to global_configuration_table_length) of std_logic_vector(15 DOWNTO 0);
	signal global_configuration_data_s: global_configuration_table;
	
	
	signal error_s							: std_logic;											-- CRC and memory compare result 0 = pass / 1 = fail

	signal speed_s							: std_logic_vector(15 downto 0);	
	signal speed_range_s 				: std_logic_vector(1 downto 0);	

	
	signal gain_s							: std_logic_vector(11 downto 0);
	signal offset_s						: std_logic_vector(15 downto 0);
	signal default_vlcp_s				: std_logic_vector(11 downto 0);
	signal max_vlcp_s						: std_logic_vector(11 downto 0);
	signal min_vlcp_s						: std_logic_vector(11 downto 0);
		
	signal eb_code_s						: integer range 0 to 31 := 0;

	
	signal els_enabled_s					: std_logic;	
	signal els_source_select_s			: std_logic_vector(2 downto 0);
--	signal speed_source_s				: std_logic;

	signal els_state_s					: std_logic;
	signal els_sources_s					: std_logic_vector(7 downto 0);
	
	signal asp_rising_threshold_s		: std_logic_vector(11 downto 0);
	signal asp_falling_threshold_s	: std_logic_vector(11 downto 0);
	
	signal relationship_select_top_bit_s : std_logic;
	signal tacho_fault_default_relationship_low_s: std_logic_vector(2 downto 0);
	signal tacho_fault_default_relationship_high_s: std_logic_vector(2 downto 0);
	
	signal 		speed_deadband_s 	: std_logic;


----------------------------------------------------------------------------------------------
	
		
begin

	-- constant outputs
	tacho_probe_select_o <= '0';

----------------------------------------------------------------------------------------------
-- Config loader
----------------------------------------------------------------------------------------------


    loader: process (clk_20mhz_i, reset_ni)
	 
		variable eb_data_count : natural range 0 to EB_CODE_LENGTH := 0;
	 
	 
    begin
        if reset_ni = '0' then
				memory_read_o 		<= '0';
				error_s				<= '1';											-- Assume error condition until state machine has run
				crc_clear_o			<= '1';
				memory_address_s	<= (others => '0');
				memory_count_s		<= (others => '0');
				
				
				overall_data_crc_o <= (others => '0');
				global_data_crc_o <= (others => '0');
				eb_id_data_crc_o <= (others => '0');
				
				config_write_o <= '0';
				
				valve_configuration_data_s <= (others =>(others => '0'));
				global_configuration_data_s <= (others =>(others => '0'));
		
				
				crc_data_o <= (others => '0');
				
				crc_load_o <= '0';
				
				eb_id_crc_error_o <= '0';
				global_data_crc_error_o <= '0';
				overall_data_crc_error_o <= '0';
				
				memory_address_o				<= (others => '0');
				error_o 							<= '0';
			
				
				state_s 				<= COMPARE_START;
				
        elsif rising_edge(clk_20mhz_i) then
 

				memory_address_o				<= memory_address_s;
				error_o 							<= error_s;



----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- First part of state machine compares the 2 sets of data in the flash to ensure they are identical and checks the overall CRC
-- Data is compared 16 bits at a time and state STOP is selected on error. No further tasks are then
-- performed until a reload is requested. If all ok, then moves to part 2.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

				case state_s is
					when COMPARE_START =>																			-- first state

						error_s 	<= '1';																				-- clear crc_error
						memory_read_o	<= '0';																		-- ensure no memory read is requested
																															-- until this process has finished before loading EB code)
						memory_address_s	<= (others => '0');													-- reset the flash memory address (assumes starts at 0)
						config_write_o <= '0';																		-- stop the mux reading the crc values
						memory_count_s	<= (others => '0');														-- reset the memory position count
						eb_id_crc_error_o <= '0';
						global_data_crc_error_o <= '0';
						overall_data_crc_error_o <= '0';

						
						crc_clear_o		<= '1';																		-- restart the crc calculator
						
						state_s 			<= COMPARE_REQUEST1;														
					when COMPARE_REQUEST1 =>																		-- state to request data from the flash memory
						memory_address_s	<= std_logic_vector(memory_count_s);													-- set the lower image memory address
						memory_read_o 	<= '1';																		-- request data from the flash memory

						crc_clear_o 	<= '0';																		-- clear any crc error ready to start again

						state_s 			<= COMPARE_WAIT_FOR_FLASH1;
					when COMPARE_WAIT_FOR_FLASH1 =>																-- wait for the flash memory read module to have data
						memory_read_o  <= '0';																		-- clear memory read flag
						if data_ready_i = '0' then																	-- wait for the data from the flash memory
							state_s <= COMPARE_WAIT_FOR_FLASH1;
						else
							state_s <= COMPARE_LOAD1;
						end if;
						-- start a load 
					when COMPARE_LOAD1 =>																			-- state to load the data from the flash module
						compare_data <= eb_data_i;																	-- copy the data to the data code checker
						crc_data_o <= eb_data_i;																	-- copy data to crc checker
						crc_load_o <= '1';																			-- 
						state_s 			<= COMPARE_REQUEST2;

					when COMPARE_REQUEST2 =>																		-- state to request data from the second copy of the data
						-- ************* test with one copy of data
--						memory_address_s	<= std_logic_vector(unsigned(memory_count_s));				-- set the lower image memory address
						memory_address_s	<= std_logic_vector(memory_count_s + IMAGE2_OFFSET);		-- set the uppper image memory address
						memory_read_o 	<= '1';																		-- request data from the flash memory

						crc_load_o <= '0';																			-- 
						
						state_s 			<= COMPARE_WAIT_FOR_FLASH2;
					when COMPARE_WAIT_FOR_FLASH2 =>																-- state to wait for the data to be available
						memory_read_o  <= '0';																		-- clear memory read flag
						if data_ready_i = '0' then																	-- wait for the data from the flash memory
							state_s <= COMPARE_WAIT_FOR_FLASH2;
						else
							state_s <= COMPARE_LOAD2;
						end if;
						-- start a load 
					when COMPARE_LOAD2 =>																			-- state to compare the data and increment memory location

						if compare_data /= eb_data_i then														-- compare upper and lower image values
							error_s <= '1';																			-- different, so set error
							overall_data_crc_error_o <= '1';
							state_s <= STOP;
						else
--							error_s <= '0';																			-- same so all ok

							memory_count_s	<= memory_count_s + 2;													-- new memory count is current value + 2 as we are reading 16 bit words
							state_s <= COMPARE_LOOP;
							
						end if;
						
						
					when COMPARE_LOOP =>																				-- state to check if all data has been compared


						if memory_count_s = CONFIG_LENGTH_BYTES - 2 then
							crc_stored_s <= crc_data_i;																-- save the current CRC
							state_s <= COMPARE_REQUEST1;															-- more data still to compare

						elsif memory_count_s = CONFIG_LENGTH_BYTES then
						
							if compare_data = crc_stored_s then													-- check if the overall CRC is correct

--							overall_data_crc_o <= crc_stored_s;														-- save the crc if correct or incorrect
								state_s <= LOAD_START;																	-- All done and ok so move to next section
							
							else

								error_s <= '1';																			-- different, so set error
								state_s <= STOP;
							
							end if;

							config_write_o <= '1';																		-- write crc values to the mux
							overall_data_crc_o <= crc_stored_s;														-- save the crc if correct or incorrect
							
						else
							state_s <= COMPARE_REQUEST1;															-- more data still to compare
						end if;
						
						
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- The second part of the state machine runs through twice. The first run loads the required EB ID code data and checks the row CRC.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

					when LOAD_START =>																				-- state to start the loading of the data to the arrays

						memory_read_o	<= '0';																		-- clear the memory read flag
						eb_data_count := 0;
						config_write_o <= '0';																		-- clear the mux write
						crc_clear_o		<= '1';																		-- reset the crc calculator
						memory_count_s	<= (others => '0');														-- reset the memory position count


						if eb_code_valid_i = '0' then																		-- wait for eb load to go high
							state_s <= LOAD_START;

						else
						
							eb_code_s <= to_integer(unsigned(eb_code_i(4 downto 1)));									-- latch the eb code
								
							state_s <= LOAD_OFFSET;
						end if;	


					when LOAD_OFFSET =>
					
							memory_address_s <= std_logic_vector(memory_count_s + (eb_code_s * EB_CODE_LENGTH * 2) );
							crc_clear_o		<= '0';																		-- ready the crc calculator
					
							state_s <= LOAD_REQUEST;
							
	
					when LOAD_REQUEST =>																				-- state to request flash data
						memory_read_o 	<= '1';																		-- request data from the flash memory
						state_s 			<= LOAD_WAIT_FOR_FLASH;
					when LOAD_WAIT_FOR_FLASH =>																	-- state to wait for data to be ready
						memory_read_o  <= '0';																		-- clear the memory read flag
						if data_ready_i = '0' then																	-- wait for the data from the flash memory
							state_s <= LOAD_WAIT_FOR_FLASH;
						else
							state_s <= LOAD_LOAD;
						end if;
						-- start a load 
					when LOAD_LOAD =>																					-- state to move to next memeory location
						memory_address_s <= std_logic_vector(unsigned(memory_address_s) + 2 );
					
						valve_configuration_data_s(eb_data_count) <= eb_data_i;
						
						crc_data_o <= eb_data_i;																	-- copy data to crc checker
						crc_load_o <= '1';																			-- 
						eb_data_count := eb_data_count + 1;													-- increment count to next position in the array
						state_s <= LOAD_CHECK_LENGTH;
						

					when LOAD_CHECK_LENGTH =>																		-- Row and column ouputs define the position of the data in the code checker array
						crc_load_o <= '0';																			-- 
	
						if eb_data_count = EB_CODE_LENGTH - 1  then												-- check if all data loaded into crc checker
							state_s <= LOAD_REQUEST_CRC;
						else
							state_s <= LOAD_REQUEST;
						end if;
	

					when LOAD_REQUEST_CRC =>																		-- get CRC from Flash
						memory_read_o <= '1';																		-- set the memory read flag
						state_s <= LOAD_WAIT_FOR_CRC;
					when LOAD_WAIT_FOR_CRC =>																		-- state to wait for the crc from the flash memeory
						memory_read_o <= '0';																		-- wait for CRC from flash
						if data_ready_i = '0' then
							state_s <= LOAD_WAIT_FOR_CRC;
						else
							state_s <= LOAD_COMPARE_CRC;
						end if;

					when LOAD_COMPARE_CRC =>																		-- state to compare the calculated crc aginst the flash memory crc
						if crc_data_i /= eb_data_i then															-- compare stored crc in flash with the calculated crc
							error_s <= '1';																			-- different, so set error
							eb_id_crc_error_o <= '1';
							state_s <= STOP;
							
						else
								state_s <= GLOBAL_START;																-- move to global data load

						
						end if;
						

						config_write_o <= '1';																		-- write crc values to the mux
						eb_id_data_crc_o <= crc_data_i;															-- store the eb id CRC if correct or incorrect

						
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- The third part of the state machine loads the global data and checks the row CRC
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

					when GLOBAL_START =>																				-- state to start the loading of the data to the arrays

						memory_read_o	<= '0';																		-- clear the memory read flag
						memory_address_s	<= (others => '0');													-- reset memory location back to the start (assumes location 0)
						eb_data_count := 0;
						crc_clear_o		<= '1';																		-- reset the crc calculator
						config_write_o <= '0';																		-- clear the mux write
						memory_count_s	<= (others => '0');														-- reset the memory position count
						
						state_s <= GLOBAL_OFFSET;

					when GLOBAL_OFFSET =>
					
							memory_address_s <= std_logic_vector(memory_count_s + (NUMBER_OF_EB_CODES * EB_CODE_LENGTH * 2) );
							crc_clear_o		<= '0';																		-- ready the crc calculator
					
							state_s <= GLOBAL_REQUEST;
	
					when GLOBAL_REQUEST =>																				-- state to request flash data
						memory_read_o 	<= '1';																		-- request data from the flash memory
						state_s 			<= GLOBAL_WAIT_FOR_FLASH;
					when GLOBAL_WAIT_FOR_FLASH =>																	-- state to wait for data to be ready
						memory_read_o  <= '0';																		-- clear the memory read flag
						if data_ready_i = '0' then																	-- wait for the data from the flash memory
							state_s <= GLOBAL_WAIT_FOR_FLASH;
						else
							state_s <= GLOBAL_LOAD;
						end if;
						-- start a load 
					when GLOBAL_LOAD =>																				-- state to move to next memeory location
						memory_address_s <= std_logic_vector(unsigned(memory_address_s) + 2 );
						global_configuration_data_s(eb_data_count) <= eb_data_i;
						crc_data_o <= eb_data_i;																	-- copy data to crc checker
						crc_load_o <= '1';																			-- 
						eb_data_count := eb_data_count + 1;														-- increment count to next position in the array
						state_s <= GLOBAL_CHECK_LENGTH;

					when GLOBAL_CHECK_LENGTH =>																	-- Row and column ouputs define the position of the data in the code checker array
						crc_load_o <= '0';																			-- 
	
						if eb_data_count = GLOBAL_DATA_LENGTH - 1  then										-- check if all data loaded into crc checker
							state_s <= GLOBAL_REQUEST_CRC;
						else
							state_s <= GLOBAL_REQUEST;
						end if;
	

					when GLOBAL_REQUEST_CRC =>																		-- get CRC from Flash
						memory_read_o <= '1';																		-- set the memory read flag
						state_s <= GLOBAL_WAIT_FOR_CRC;
					when GLOBAL_WAIT_FOR_CRC =>																		-- state to wait for the crc from the flash memeory
						memory_read_o <= '0';																		-- wait for CRC from flash
						if data_ready_i = '0' then
							state_s <= GLOBAL_WAIT_FOR_CRC;
						else
							state_s <= GLOBAL_COMPARE_CRC;
						end if;

					when GLOBAL_COMPARE_CRC =>																		-- state to compare the calculated crc aginst the flash memory crc
						if crc_data_i /= eb_data_i then															-- compare stored crc in flash with the calculated crc
							error_s <= '1';																			-- different, so set error
							global_data_crc_error_o <= '1';
						else
							error_s <= '0';																			-- all ok
							
																
						end if;
						
						global_data_crc_o <= crc_data_i;															-- store the CRC for reading by the SPI interface if correct or incorrect
						config_write_o <= '1';																		-- write crc values to the mux
						state_s <= STOP;																				-- all done so stop
						
					when STOP =>																					-- state to wait at as we are finished. 
						config_write_o <= '0';																	-- stop the mux reading the crc values
						if eb_code_valid_i = '0' then															-- start again if eb code becomes invalid
							state_s <= COMPARE_START;
						else
							state_s <= STOP;																		-- finished, so wait here
						end if;

					when others =>
						state_s <= STOP;																			-- something went wrong so go to STOP state and wait for reload
							
					end case;						

				end if;
			end process;
			
			
			
----------------------------------------------------------------------------------------------
-- Load parameters from config data
----------------------------------------------------------------------------------------------
load_paramters: process (clk_20mhz_i, reset_ni)
	 
	 
	begin

		if reset_ni = '0' then

				asp1_transducer_source_o 					<= (others => '0');
				asp2_transducer_source_o 					<= (others => '0');
				gain_s 											<= (others => '0');
				offset_s 										<= (others => '0');
				default_vlcp_s 								<= (others => '0');
				max_vlcp_s 										<= (others => '0');
				min_vlcp_s 										<= (others => '0');
				lw_fpga_op_enable_o 							<= '0';
				tacho_fault_default_relationship_low_s <= (others => '0');
				tacho_fault_default_relationship_high_s <= (others => '0');
				els_state_s 									<= '0';
				asp_falling_threshold_s 					<= (others => '0');
				asp_rising_threshold_s 						<= (others => '0');
				relationship_o									<= (others => '0');
				els_sources_s									<= (others => '0');


       elsif rising_edge(clk_20mhz_i) then
			
			els_sources_s 					<= ex_digital_inputs_i & external_lbsr_trip_lw_fpga_els;
			
			gain_o 							<= gain_s;
			offset_o 						<= offset_s;
			default_vlcp_o 				<= default_vlcp_s;
			max_vlcp_o						<= max_vlcp_s;
			min_vlcp_o						<= min_vlcp_s;
			asp_rising_threshold_o		<= asp_rising_threshold_s;
			asp_falling_threshold_o 	<= asp_falling_threshold_s;
			

			vent_timer_reset_mode_o		<= global_configuration_data_s(0)(0);
			ceb_enabled_o					<= global_configuration_data_s(0)(1);
			internal_lbsr_enabled_o 	<= global_configuration_data_s(0)(2);
			external_lbsr_enabled_o 	<= global_configuration_data_s(0)(3);
			els_source_select_s			<= global_configuration_data_s(0)(7 downto 5);
			eb_id_quantity_o				<= global_configuration_data_s(0)(11 downto 8);
			data_version_o  				<= global_configuration_data_s(1)(7 downto 0);
			dataset_format_version_o	<= global_configuration_data_s(1)(15 downto 8);

			project_identifier_o  		<= global_configuration_data_s(2) & global_configuration_data_s(3);
			
			ceb_variables_1_o				<= global_configuration_data_s(4);
			ceb_variables_2_o				<= global_configuration_data_s(5);
			ceb_variables_3_o				<= global_configuration_data_s(6);

			hold_off_wsp_timing_o 		<= global_configuration_data_s(7)(15 downto 12);
			hold_timeout_wsp_timing_o 	<= global_configuration_data_s(7)(11 downto 8);
			vent_off_wsp_timing_o 		<= global_configuration_data_s(7)(7 downto 4);
			vent_timeout_wsp_timing_o 	<= global_configuration_data_s(7)(3 downto 0);
			
			eb_id_indication_o			<= valve_configuration_data_s(0)(15 downto 12);
			els_enabled_s					<= valve_configuration_data_s(0)(7);
			
			
					
----------------------------------------------------------------------------------------------
-- An error has occurd at some point, or the state machine has not yet run, so the default 
-- values are loaded
					
			if error_s = '1' then																					

				gain_s 											<= GAIN_DEFAULT;
				offset_s 										<= OFFSET_DEFAULT;
				default_vlcp_s									<= VLCP_DEFAULT;
				max_vlcp_s										<= VLCP_DEFAULT;
				min_vlcp_s										<= VLCP_DEFAULT;
				asp1_transducer_source_o 					<= (others => '0');
				asp2_transducer_source_o 					<= (others => '0');
				lw_fpga_op_enable_o 							<= '0';
				tacho_fault_default_relationship_low_s <= (others => '0');
				tacho_fault_default_relationship_high_s <= (others => '0');
				els_state_s 									<= '0';
				asp_falling_threshold_s 					<= (others => '0');
				asp_rising_threshold_s 						<= (others => '0');
				relationship_o									<= (others => '0');
				
			else
			
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- No local errors, so we can now load all the required parameters 


				lw_fpga_op_enable_o <= '1';																		-- all ok so set the ouptut. This drives part of the 24V safety interlock

				-- if enabled the els state = the value of the select input else its zero
				els_state_s <= els_sources_s(to_integer(unsigned(els_source_select_s))) and els_enabled_s;
			
				
				relationship_o <= relationship_select_top_bit_s & speed_range_s;
				
				debug0_o <= speed_deadband_s & "000000000000" & relationship_select_top_bit_s & speed_range_s;
				debug1_o <= "0000000000000" & tacho_fault_default_relationship_low_s;
				debug2_o <= "0000000000000" & tacho_fault_default_relationship_high_s;
				
				
				-- select the gain value from the configuration data
				gain_s <= valve_configuration_data_s( ( to_integer(unsigned'("" & relationship_select_top_bit_s)) * RELATIONSHIP_LENGTH * 4) + 
							 (to_integer(unsigned(speed_range_s)) * RELATIONSHIP_LENGTH) + RELATIONSHIP_GAIN_OFFSET )(11 downto 0);
													
				-- select the offset value from the configuration data
				offset_s <= valve_configuration_data_s((to_integer(unsigned'("" & relationship_select_top_bit_s)) * RELATIONSHIP_LENGTH * 4) + 
								(to_integer(unsigned(speed_range_s)) * RELATIONSHIP_LENGTH) + RELATIONSHIP_OFFSET_OFFSET)(15 downto 0);
				-- select the minimum vlcp value from the configuration data
				min_vlcp_s <= valve_configuration_data_s((to_integer(unsigned'("" & relationship_select_top_bit_s)) * RELATIONSHIP_LENGTH * 4) + 
								 (to_integer(unsigned(speed_range_s)) * RELATIONSHIP_LENGTH) + RELATIONSHIP_MIN_VLCP_OFFSET)(11 downto 0);

				-- select the maxmum vlcp value from the configuration data
				max_vlcp_s <= valve_configuration_data_s((to_integer(unsigned'("" & relationship_select_top_bit_s)) * RELATIONSHIP_LENGTH * 4) + 
									(to_integer(unsigned(speed_range_s)) * RELATIONSHIP_LENGTH) + RELATIONSHIP_MAX_VLCP_OFFSET)(11 downto 0);

				-- select the default vlcp value from the configuration data
				default_vlcp_s <= valve_configuration_data_s((to_integer(unsigned'("" & relationship_select_top_bit_s)) * RELATIONSHIP_LENGTH * 4) + 
										(to_integer(unsigned(speed_range_s)) * RELATIONSHIP_LENGTH) + RELATIONSHIP_DEFAULT_VLCP_OFFSET)(11 downto 0);
											
				-- select the asp 1 transducer source from the configuration data
				asp1_transducer_source_o <= valve_configuration_data_s(EB_ID_POSITION)(2 downto 0);

				-- select the asp 2 transducer source from the configuration data
				asp2_transducer_source_o <= valve_configuration_data_s(EB_ID_POSITION)(5 downto 3);
			
				tacho_fault_default_relationship_low_s <= valve_configuration_data_s(LOW_ASP_FALLING_THRESHOLD_POSITION)(14 downto 12);
				tacho_fault_default_relationship_high_s <= valve_configuration_data_s(LOW_ASP_RISING_THRESHOLD_POSITION)(14 downto 12);

				asp_falling_threshold_s <= valve_configuration_data_s(LOW_ASP_FALLING_THRESHOLD_POSITION)(11 downto 0);
				asp_rising_threshold_s <= valve_configuration_data_s(LOW_ASP_RISING_THRESHOLD_POSITION)(11 downto 0);
			
			end if;
						
        end if;
    end process;


----------------------------------------------------------------------------------------------
-- select speed range
----------------------------------------------------------------------------------------------

    speed_select: process (clk_20mhz_i, reset_ni)
	 
	 
    begin

		if reset_ni = '0' then

			speed_s <= (others => '0');
			speed_range_s <= (others => '0');
			speed_deadband_s <= '0';
			
				
       elsif rising_edge(clk_20mhz_i) then
		 
		 
			if error_s = '1' then																					
				speed_s <= (others => '0');
				speed_range_s <= (others => '0');
		 
			else
		 
				-- select tacho 0 to tacho 1 frequency based on speed source
				if tacho_frequency0_i > tacho_frequency1_i then
					speed_s <= tacho_frequency0_i;
				else
					speed_s <= tacho_frequency1_i;
				end if;
				
				-- if tacho not healthy (0), an error has occured elsewhere in the system and the relationship defined in the config table must be used
				if tacho_healthy_i = '0' then

					if els_state_s = '0' then
					
						relationship_select_top_bit_s <= tacho_fault_default_relationship_low_s(2);
						
						speed_range_s <= tacho_fault_default_relationship_low_s(1 downto 0);
						
					else
						relationship_select_top_bit_s <= tacho_fault_default_relationship_high_s(2);

						speed_range_s <= tacho_fault_default_relationship_high_s(1 downto 0);
					
					end if;
					
				else
				
					relationship_select_top_bit_s <= els_state_s;
					
					if pulse_1ms_i = '1' then 

						speed_deadband_s <= '0';
					
					
						-- speed switch = 0
						if speed_s <= valve_configuration_data_s(SPEED_SWITCH0_FALLING_THRESHOLD_POSITION) then		
							speed_range_s <= "00";
							
						-- speed switch = 1
						elsif speed_s > valve_configuration_data_s(SPEED_SWITCH0_RISING_THRESHOLD_POSITION) and		
								speed_s < valve_configuration_data_s(SPEED_SWITCH1_FALLING_THRESHOLD_POSITION) then
							speed_range_s <= "01";
						-- speed switch = 2
						elsif speed_s > valve_configuration_data_s(SPEED_SWITCH1_RISING_THRESHOLD_POSITION) and		
								speed_s < valve_configuration_data_s(SPEED_SWITCH2_FALLING_THRESHOLD_POSITION) then
							speed_range_s <= "10";
						-- speed switch = 4
						elsif speed_s > valve_configuration_data_s(SPEED_SWITCH2_RISING_THRESHOLD_POSITION) then	
							speed_range_s <= "11";
						-- else hold value
						else
							speed_range_s <= speed_range_s;
							speed_deadband_s <= '1';
						end if;
						
						
					end if;
							
				end if;

				
			end if;
		 
		 end if;
 
	end process;


	 
	 
end architecture imp_eb_config_loader;

