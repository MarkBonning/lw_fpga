-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  spi_bypass.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning
--                     Date  :  05/02/2016
--
-- -------------------------------------------------------------------------------
-- description
-- Mux to bypass the spi interface and allow the Aurix processor to connect
-- directly to the flash memory part.
-- -------------------------------------------------------------------------------
-- revision 00
--
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity spi_bypass is
	port 	(
		clock20mhz_i       	: in  std_logic;
      reset_ni           	: in  std_logic;

		spi_bypass_command_i	: in std_logic_vector(15 downto 0);															

		spi_clk_aurix_i     	: in std_logic;  	
		spi_clk_flash_i    	: in std_logic; 	
		spi_clk_fpga_o     	: out std_logic;  	
		spi_clk_flash_o    	: out std_logic; 	


		spi_cs_aurix_i     	: in std_logic;  	
		spi_cs_flash_i    	: in std_logic; 	
		spi_cs_fpga_o     	: out std_logic;  	
		spi_cs_flash_o    	: out std_logic; 	


		spi_mosi_aurix_i    	: in std_logic;  	
		spi_mosi_flash_i    	: in std_logic; 	
		spi_mosi_fpga_o     	: out std_logic;  	
		spi_mosi_flash_o    	: out std_logic;

		spi_miso_aurix_o    	: out std_logic;  	
		spi_miso_flash_o    	: out std_logic; 	
		spi_miso_fpga_i     	: in std_logic;  	
		spi_miso_flash_i    	: in std_logic;
		
		pulse_1ms_i				: std_logic
		 

        );
end;

architecture RTL of spi_bypass is

---- Constants -------------------------------------------------------------------------------
	constant spi_bypass_timeout_c	: integer := 20000;
	
	constant spi_bypass_command_c	: std_logic_vector(15 downto 0) := x"55aa";
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	signal spi_bypass_s : std_logic;

	signal spi_bypass_timeout_s	: std_logic;
	
	-- currently 20 second timeout (easier for testing on hardware)
	signal spi_bypass_counter_s	: integer range 0 to spi_bypass_timeout_c	:= 0;

----------------------------------------------------------------------------------------------
	
begin


	-- map signals to ports

			
			spi_clk_fpga_o <= spi_clk_aurix_i when spi_bypass_s = '0' else '0';
			spi_clk_flash_o <= spi_clk_flash_i when spi_bypass_s = '0' else spi_clk_aurix_i;
			
			spi_cs_fpga_o <= spi_cs_aurix_i when spi_bypass_s = '0' else '1';
			spi_cs_flash_o <= spi_cs_flash_i when spi_bypass_s = '0' else spi_cs_aurix_i;
			
			spi_mosi_fpga_o <= spi_mosi_aurix_i when spi_bypass_s = '0' else '0';
			spi_mosi_flash_o <= spi_mosi_flash_i when spi_bypass_s = '0' else spi_mosi_aurix_i;

					
			spi_miso_aurix_o <= spi_miso_fpga_i when spi_bypass_s = '0' else spi_miso_flash_i;
			spi_miso_flash_o <= spi_miso_flash_i when spi_bypass_s = '0' else '0';
				

	mux_write : process (reset_ni, clock20mhz_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			spi_bypass_s <= '0';
			
			spi_bypass_counter_s <= 0;

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20mhz_i) then	 
				

			-- set the bypass condition for Aurix to flash read / write
			if spi_bypass_command_i = spi_bypass_command_c and spi_bypass_timeout_s = '0' then
				spi_bypass_s <= '1';
				
				-- increase the count on the 1ms pulse
				if spi_bypass_counter_s = spi_bypass_timeout_c then
					spi_bypass_timeout_s <= '1';
				elsif pulse_1ms_i = '1' then
					spi_bypass_counter_s <= spi_bypass_counter_s + 1;
				end if;
				
			-- clear timeout counter whenever aurix chip select goes low
			-- **** MIGHT NEED TO DELETE THIS !!!!!!
			elsif spi_cs_aurix_i = '0' then

				spi_bypass_counter_s <= 0;				
				
			-- otherwise clear bypass
			else
				spi_bypass_s <= '0';
				
				-- only clear timeout and counter is command is cleared away from the bypass command
				-- this stops the bypass reactivating itself immediately after the previous bypass
				if spi_bypass_command_i /= spi_bypass_command_c then
					spi_bypass_timeout_s <= '0';
					spi_bypass_counter_s <= 0;				
				end if;
				
			end if;

				
				
	 	end if;
	end process;


end architecture RTL;