-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_interface.vhd
--                   Author  :  Mark Bonning 
--                     Date  :  30/09/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Interface to the internal adc hardcore
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. 
--             Changes to channel control       
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use work.pvuConstants.all;

entity adc_interface is
	port 	(
		clock20mhz_i     	:  in std_logic; 
		reset_ni  			    	:  in std_logic;

		reset_sink_reset_n  	: out std_logic;
		response_valid      	:  in std_logic;
		response_channel    	:  in std_logic_vector( 4 downto 0);
		response_data       	:  in std_logic_vector(11 downto 0);
		sequencer_address   	: out std_logic;
		sequencer_write     	: out std_logic;
		sequencer_writedata 	: out std_logic_vector(31 downto 0);

		fpga_diode_temperature_o	: out std_logic_vector(15 downto 0)

		
		);
end;

architecture RTL of adc_interface is

	
	
	type state_cm_wr is ( ready, adc_start, adc_run );

	signal set_sync_sm     :	state_cm_wr;


begin

	reset_sink_reset_n <= '1';
	sequencer_address  <= '0';

	-- receive data from adc
	receive_data :process (reset_ni, clock20mhz_i) is
	begin
		if reset_ni = '0' then
			fpga_diode_temperature_o <=  ( others => '0' );
			
		elsif rising_edge ( clock20mhz_i ) then

			if response_valid = '1' then -- valid for one clock cycle
				fpga_diode_temperature_o <=  "0000" & response_data;
			end if;
		end if;
	end process;



	
-- sets the adc start signal. runs once only
	process (reset_ni, clock20mhz_i) is
		variable count : integer range 0 to 10:= 0;
	begin
		if reset_ni = '0' then
			set_sync_sm <= READY;
			sequencer_write 		<= '0';
			sequencer_writedata 	<=  (others => '0') ;
		elsif Rising_Edge ( clock20mhz_i ) then
			case set_sync_sm is
				when READY =>
					count := count + 1;
					if count = 9 then
						set_sync_sm      		<= adc_start;
					else
						set_sync_sm      		<= READY;
					end if;
				when adc_start =>
					sequencer_write 			<= '1';
					sequencer_writedata(0) 	<= '1';
					set_sync_sm      			<= adc_run;
				when adc_run =>
					sequencer_write 			<= '0';
					sequencer_writedata 		<= (others => '0') ;
					set_sync_sm  				<= adc_run;
			end case;
		end if;
	end process;


end architecture RTL;