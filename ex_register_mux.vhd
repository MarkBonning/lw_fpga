-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pvu_register_mux.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  21/02/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register mux. Multiplexer for the spi interface to the ex registers
-- Load weigh is the SPI master and requests data form the EX fpga
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ex_register_mux is
	port 	(
	
		clock20m_i						: in std_logic;		
		reset_ni							: in std_logic;		
		
		-- Output registers
		register_0_o					: out std_logic_vector(15 downto 0); 	-- 
		register_1_o					: out std_logic_vector(15 downto 0); 	-- 
		register_2_o					: out std_logic_vector(15 downto 0); 	-- 
		register_3_o					: out std_logic_vector(15 downto 0); 	-- 
		register_4_o					: out std_logic_vector(15 downto 0); 	-- 
		register_5_o					: out std_logic_vector(15 downto 0); 	-- 

		
		spi_clk_o						: out std_logic;
		spi_cs_o							: out std_logic;
		spi_mosi_o						: out std_logic;
		spi_miso_i						: in std_logic;
		
		
		ex_spi_read_error				: out std_logic;

		
		pulse_1ms_i						: in std_logic												-- single 20mhz clock pulse at 1khz (1ms)

		
		
		
		);
end; 

architecture RTL of ex_register_mux is

	
---- Constants -------------------------------------------------------------------------------
	
--	constant number_of_bits_c		: integer := 111;
	
----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------
	

	signal spi_clk_counter_s		: std_logic_vector(7 downto 0) ;
	
	signal slow_clk_s					: std_logic;
	
	signal spi_clk_s					: std_logic;

	-- spi transmit buffer (address copy, data and crc)
	signal rx_data_s  				: std_logic_vector(0 to 111);  					
	
	signal rx_bit_count_s 			: unsigned(7 downto 0);
	
	signal spi_cs_s					: std_logic;
	
	signal start_s						: std_logic;
	
	signal busy_s						: std_logic;
	
	
type states is ( START, CHIP_ENABLE, CLOCK_DATA, CLOCK_DONE, CHECK_XSUM, LOAD_REGISTERS);
signal state_s   	: states;
	
----------------------------------------------------------------------------------------------
	
	
begin
		
	--map registers to ports

	-- slow clock for spi (divider from main 20mhz)
	slow_clk_s <= spi_clk_counter_s(7);
	
	-- mosi is always 0
	spi_mosi_o <= '0';
	
	spi_cs_o <= spi_cs_s;
	
	spi_clk_s <= slow_clk_s and not spi_cs_s;
	
	spi_clk_o <= spi_clk_s;

-----------------------------------------------------------------------------------------------------------	
-- spi clock generator
-----------------------------------------------------------------------------------------------------------
	clock_gen : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			
			spi_clk_counter_s <= (others => '0');
			
			start_s <= '0';
			
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 
		
			spi_clk_counter_s <= std_logic_vector(unsigned(spi_clk_counter_s) + 1);
			

			-- pulse_1ms is only 1 20mhz clock cycle long, so need to monitor in a process clocked at 20mhz
			if	busy_s = '0' then
				if pulse_1ms_i = '1' then
					start_s <= '1';
				end if;
			else
				start_s <= '0';
			end if;
		
		end if;
	end process;
	
	
	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPI RX
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	spi_rx : process (spi_cs_s, spi_clk_s)
	
	
	begin
	-- Use the chip select signal to clear all the counts and flags
		if spi_cs_s = '1' then
	
			if state_s = START then
				
				rx_data_s 					<= (others => '0');
				
			end if;

			rx_bit_count_s 			<= (others => '0');
			

		-- clock the process o nthe ruising edge of the SPI clock
		elsif rising_edge (spi_clk_s) then

			-- load the rx register
			rx_data_s(to_integer(rx_bit_count_s)) <= spi_miso_i;

			-- increment the bit count each lap as long as we dont over run the buffers.
			--if you send an SPI message with extra bits / bytes this will stop it from crashing
			if rx_bit_count_s < 111 then
				rx_bit_count_s <= rx_bit_count_s + 1;
			end if;
					
			
		end if;

	end process;
	

	
-----------------------------------------------------------------------------------------------------------	
-- transmit data to spi module from registers
-----------------------------------------------------------------------------------------------------------
	mux_read : process (reset_ni, slow_clk_s)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			
			register_0_o <= (others => '0');
			register_1_o <= (others => '0');
			register_2_o <= (others => '0');
			register_3_o <= (others => '0');
			register_4_o <= (others => '0');
			register_5_o <= (others => '0');

			spi_cs_s	<= '1';

			ex_spi_read_error <= '0';
			
			busy_s <= '0';
			
			state_s <= START;
			
			
		-- clock on rising edge of slow clock
		elsif rising_edge(slow_clk_s) then	 

			
			-------------------------------------------------------------------------------------------------------------------------------------------------------------------
			-- Perform SPI read of EX card
			-------------------------------------------------------------------------------------------------------------------------------------------------------------------

				case state_s is

					-- start condition
					when START =>				
					
						spi_cs_s	<= '1';
					
					
						
						if	start_s = '1' then
						
							busy_s <= '1';

							
							state_s <= CHIP_ENABLE;
						else

							busy_s <= '0';
						
							state_s <= START;

						end if;
						
					when CHIP_ENABLE =>
					
						spi_cs_s	<= '1';
					
						state_s <= CLOCK_DATA;
						

					when CLOCK_DATA =>

						spi_cs_s	<= '0';
				

						if rx_bit_count_s = 111 then
							state_s <= CLOCK_DONE;
						else
							state_s <= CLOCK_DATA;
						end if;
						
					
					when CLOCK_DONE =>
					
						spi_cs_s	<= '1';
					
						state_s <= CHECK_XSUM;
						

					when CHECK_XSUM =>
					
						if rx_data_s(96 to 111) = std_logic_vector(	unsigned(rx_data_s(0 to 15)) + unsigned(rx_data_s(16 to 31)) + 
																					unsigned(rx_data_s(32 to 47)) + unsigned(rx_data_s(48 to 63)) + 
																					unsigned(rx_data_s(64 to 79)) + unsigned(rx_data_s(80 to 95))) then
																					
							ex_spi_read_error <= '0';

							state_s <= LOAD_REGISTERS;

						else

							ex_spi_read_error <= '1';

							state_s <= START;
						end if;

						
					when LOAD_REGISTERS =>
						
						register_0_o <= rx_data_s(0 to 15);
						register_1_o <= rx_data_s(16 to 31);
						register_2_o <= rx_data_s(32 to 47);
						register_3_o <= rx_data_s(48 to 63);
						register_4_o <= rx_data_s(64 to 79);
						register_5_o <= rx_data_s(80 to 95);

						state_s <= START;
							
					-- something went wrong, start again
					when others =>
						state_s <= START;
				end case;		          

			
			
			
	 	end if;
	end process;
	
	
end architecture RTL;