-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  LW FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  valve_control.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  19/02/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Sets the state of the inlet and vent valves from the data and configuration provided
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.02: Mark Bonning 03/02/2020 -- changed calulation range from 5 down 2
-- Issue 0.01: Mark Bonning (Based on inital design by Leesa Kingman)
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.pvuconstants.all;



entity valve_control is
    port (
		clock20mhz_i						: in  std_logic;
      reset_ni								: in  std_logic;
		
		gain_i								: in std_logic_vector(11 downto 0);								-- selected EB gain
		offset_i								: in std_logic_vector(11 downto 0);								-- selected EB offset

		default_vlcp_i						: in std_logic_vector(11 downto 0);
		max_vlcp_i							: in std_logic_vector(11 downto 0);
		min_vlcp_i							: in std_logic_vector(11 downto 0);
		
	
		inlet_mv_o							: out std_logic;														-- inlet magnetic valve
		vent_mv_o							: out std_logic;														-- vent magnetic valve
		
		vent_mv_operations_counter_msb_o		: out std_logic;													-- msb of the vent operations counter
		inlet_mv_operations_counter_msb_o	: out std_logic;												-- msb of the vent operations counter
		
		emergency_1					: in std_logic;
		emergency_2					: in std_logic;
		
		vlcp_target_o						: out std_logic_vector(15 downto 0);
		
		vlcp_i								: in std_logic_vector(11 downto 0);								-- current vlcp input from adc
		
		low_asp_1_i 						: in std_logic;
		low_asp_2_i 						: in std_logic;
		asp_average_i						: in std_logic_vector(11 downto 0);								-- select asp (averaged)
		
		vlcp_transducer_fault_i			: in std_logic;
		asp_1_transducer_fault_i		: in std_logic;
		asp_2_transducer_fault_i		: in std_logic;
		
		pulse_1ms_i							: in std_logic;
		
		vlcp_deadband_upper_o : out std_logic_vector(11 downto 0);	
		vlcp_deadband_lower_o : out std_logic_vector(11 downto 0)

		
		
		  
		  
    );
end entity valve_control;

architecture imp_valve_control of valve_control is

---- Constants -------------------------------------------------------------------------------

		constant counter_limit_450ms_c 				: integer := 450;				-- Counter limit to give a 450ms delay for the vent valve
		constant counter_limit_50ms_c 				: integer := 50;				-- Counter limit to give a 50ms delay for the vent valve

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

		-- State machine states
		type state_code_latch is ( WAIT_1MS, CALCULATE, CHECK_LIMITS  );
		signal state_s     :	state_code_latch;
		
		signal vlcp_target_s							: unsigned(11 downto 0);									-- the calulated target or selected target vlcp
		signal vlcp_calculated_s					: unsigned(35 downto 0);									-- the calulated target vlcp
		
		signal calculate_vclp_target				: std_logic;													-- flag to start calculation of vlcp target
		signal compare_vclp_target_s				: std_logic;													-- flag to start compare of vlcp target and actual
		
		signal vlcp_deadband_plus_s 				: unsigned(11 downto 0);									-- stores for the vlcp with dead bands added / substracted)
		signal vlcp_deadband_minus_s				: unsigned(11 downto 0);									--
		signal vlcp_deadband_upper_s				: unsigned(11 downto 0);									--
		signal vlcp_deadband_lower_s				: unsigned(11 downto 0);									--
		
		signal vent_mv_s								: std_logic;													-- vent magnetic valve drive signal
		signal vent_mv_request_s					: std_logic;													-- flag to request valve operation
		signal vent_mv_counter_flag_s				: std_logic;													-- single clock cycle flag to trigger the operations counter
		signal vent_mv_operations_counter_s		: unsigned (10 downto 0);									-- counter for the number of vent valve operations

		signal inlet_mv_s								: std_logic;													-- inlet magnetic valve drive signals
		signal inlet_mv_request_s					: std_logic;													-- flag to request valve operation
		signal inlet_mv_counter_flag_s			: std_logic;													-- single clock cycle flag to trigger the operations counter
		signal inlet_mv_operations_counter_s	: unsigned (10 downto 0);									-- counter for the number of inlet valve operations

		signal counter_450ms_s 						: integer range 0 to counter_limit_450ms_c := 0;	-- counter to hold the current 450ms delay count
		signal counter_50ms_s 						: integer range 0 to counter_limit_50ms_c := 0;		-- counter to hold the current 50ms delay count
		
	
----------------------------------------------------------------------------------------------
		

begin



-------------------------------------------------------------------------------------------------------
-- This process calculates the taget vlcp usinG the currently selected asp value(s), gain and offset
-------------------------------------------------------------------------------------------------------

target_vlcp:
    process (clock20mhz_i, reset_ni)
    begin
        -- set initial conditions under reset
        if reset_ni = '0' then

			compare_vclp_target_s			<= '0';
			vlcp_target_s 						<= (others => '0');
			vlcp_calculated_s 				<= (others => '0');
			vlcp_target_o 						<= (others => '0');
			
			state_s								<= WAIT_1MS;
				
        elsif rising_edge(clock20mhz_i) then
		  
			-- map signals to ports
			vlcp_target_o <= "0000" & std_logic_vector(vlcp_target_s);
		  
 
			-- test the current FSM state
			case state_s is
				

-------------------------------------------------------------------------------------------------------------------------------------------------------------------				
-- Wait here for 1ms pulse to start the process

				when WAIT_1MS =>

					-- clear the compare flag, so that we only compare after an update
					compare_vclp_target_s <= '0';
	
					if pulse_1ms_i = '1'then
 
						state_s <= CALCULATE;

					else
				
						state_s <= WAIT_1MS;
					end if;
 				

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--******** This all needs checking sinse changing to max range of 2 not 5
--
-- Perform the calculation to obtain the vlcp value
-- vlcp = asp * gain + offset
-- The values are stored as 12 bit numbers where the 12 bits gives the range of 0 to 2 (was 5).
-- Therefore, the LSB = 2/4095 = 0.0004884
-- For example a gain of 0.303 is storeed as 0.303 / 0.0004884 = 620
-- To enable using just integer maths in the fpga and to allow for gain values of less than 1, then the following formula is used:
-- vlcp = (asp * gain * 488) / 1000000
-- For example:
-- asp crush of 1.890 in bits gives 1.890 / 0.000488 = 3872
-- gain of 0.303 in bits gives 0.303 / 0.000488 = 620
-- offset of 0.705 in bits gives 0.705 / 0.000488 = 1444
-- so
-- vlcp = ((3872 * 620 * 488) / 1000000) + 1444 = 2615
-- The processor will then be able to read this value and convert back to a real vclp value of 2615 * 0.000488 = 0.853
-- Note there will be some rounding errors created by both the encoding process (Excel) and the decoding process (fpga)
-- The offset can be a negative number and this is defined by setting the top bit to a 1. If this top bit is 1, then the formula 
-- subtracts the offset (excluding top bit) 
				when CALCULATE =>
					
						-- if top bit of offset is clear, then offset is positive
						if offset_i(11) = '0' then
						
							-- calculate the vlcp = (gain * asp) + offset
							vlcp_calculated_s <= (((unsigned(gain_i) * 488 * (unsigned(asp_average_i) - transducer_offset_c) ) / 1000000) + unsigned(offset_i)); 	
							
						-- else offset is negative, so subtract offset excluding top bit
						else
						
							-- calculate the vlcp = (gain * asp) - offset
							vlcp_calculated_s <= (((unsigned(gain_i) * 488 * (unsigned(asp_average_i)) - transducer_offset_c) / 1000000) - unsigned(offset_i(10 downto 0))); 	
							
						end if;
						
						state_s <= CHECK_LIMITS;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- *** NOTE this has changed. Max and min enables are no longer selected by enables

-- check if the calculates vlcp is within the range of the max and min vlcp when they are enambled.
-- if not in limits, then set to max or min accordingly
-- otherwise, take as is.

				when CHECK_LIMITS =>													
						

					-- if there is a fault with the selected asp transducers, 
					-- or if the calculated vlcp is greater than the max allowed, 
					-- or if the calculated vlcp is less than the min allowed, 
					-- then load the deafult vlcp value
					if asp_1_transducer_fault_i = '1' or asp_2_transducer_fault_i = '1' or 
						vlcp_calculated_s(11 downto 0) > unsigned(max_vlcp_i) or 
						vlcp_calculated_s(11 downto 0) < unsigned(min_vlcp_i) then
		
						vlcp_target_s <= unsigned(default_vlcp_i);
				
					-- else, use calculated vlcp
					else
					
						vlcp_target_s <= vlcp_calculated_s(11 downto 0);
						
					end if;
					
					-- tell the compare process to run
					compare_vclp_target_s <= '1';
					state_s <= WAIT_1MS;
				
				when others =>
					state_s <= WAIT_1MS;
			end case;
 			
        end if;
    end process;
    
    
-------------------------------------------------------------------------------------------------------------
-- compare target vlcp with measured vlcp range upper and vlcp range lower then set inlet and vent magnets
-------------------------------------------------------------------------------------------------------------
	vlcp_compare : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			inlet_mv_request_s <= '0' ;
			vent_mv_request_s <= '0';
		elsif rising_edge (clock20mhz_i) then
			-- compare the asp value to the vlcp deadband range when compare is set
			if compare_vclp_target_s = '1' then
				
				-- if there is a fault with the vlcp transduser, or low asp, drive the inlet only to crush
				if vlcp_transducer_fault_i = '1' or low_asp_1_i = '1' or low_asp_2_i = '1'  then
					inlet_mv_request_s 		<= '1';
					vent_mv_request_s 		<= '0';

					-- if vclp plus deadband is less than vlcp target then set inlet only
				elsif vlcp_deadband_lower_s < vlcp_target_s then
					inlet_mv_request_s 		<= '1';
					vent_mv_request_s 		<= '0';
				
				-- if vclp plus deadband greater than the target then set vent/vent only
				elsif vlcp_deadband_upper_s > vlcp_target_s then
					inlet_mv_request_s 		<= '0';
					vent_mv_request_s 		<= '1';

					-- asp value is within the range then inlet is instantly set back to '0'
					--, asp vent set to '0' counter is started
				else
					inlet_mv_request_s 		<= '0';
					vent_mv_request_s 		<= '0';
				end if;
			end if;
		end if;
	end process;
	
	

-------------------------------------------------------------------------------------------
-- Add deadband values depending on if in emergency and current inlet/ vent settings
-- if not already changing the pressure then value to set limits.
-------------------------------------------------------------------------------------------
	vlcp_deadband_calc : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			vlcp_deadband_plus_s <= ( others => '0' ) ;
			vlcp_deadband_minus_s <= ( others => '0' );
			vlcp_deadband_upper_s <= ( others => '0' );
			vlcp_deadband_lower_s <= (others => '0');
			
			
		elsif rising_edge ( clock20mhz_i ) then
		
			-- emergency condition
			-- Deadband setting when in Emergency condition
			if emergency_1 = '0' or emergency_2 = '0' then										

				-- Deadband when no current valve request
				if inlet_mv_request_s = '0' and vent_mv_request_s = '0'  then							
					vlcp_deadband_plus_s 		<= VLCP_DEADBAND_PLUS_MAX_EMER;
					vlcp_deadband_minus_s 		<= VLCP_DEADBAND_MINUS_MIN_EMER;
				
				-- Deadband when either valve request is set
				else																										
					vlcp_deadband_plus_s 		<= VLCP_DEADBAND_PLUS_UPPER_EMER;
					vlcp_deadband_minus_s 		<= VLCP_DEADBAND_MINUS_LOWER_EMER;
				end if;
			
			-- not in emergency condition
			-- Deadband setting when not in Emergency condition
			else																											
				-- Deadband when no current valve request
				if inlet_mv_request_s = '0' and vent_mv_request_s = '0'  then							
					vlcp_deadband_plus_s 		<= VLCP_DEADBAND_PLUS_MAX;
					vlcp_deadband_minus_s 		<= VLCP_DEADBAND_MINUS_MIN;
				
				-- Deadband when either valve request is set
				else																										
					vlcp_deadband_plus_s 		<= VLCP_DEADBAND_PLUS_UPPER;
					vlcp_deadband_minus_s 		<= VLCP_DEADBAND_MINUS_LOWER;
				end if;
			end if;

			-- set deadband range ----------------------------------------------
			vlcp_deadband_upper_s <= vlcp_deadband_plus_s + unsigned(vlcp_i);
			
			vlcp_deadband_upper_o <= std_logic_vector(vlcp_deadband_upper_s);
			vlcp_deadband_lower_o <= std_logic_vector(vlcp_deadband_lower_s);
			
			-- only subtract if value would remain positive
			if unsigned(vlcp_i) > vlcp_deadband_minus_s then
				vlcp_deadband_lower_s <=  unsigned(vlcp_i) - vlcp_deadband_minus_s;    
			-- else set to aero
			else
				vlcp_deadband_lower_s <= (others => '0');
			end if;
			--------------------------------------------------------------------
	
	
		end if;
	end process;

	
	
    
--------------------------------------------------------------------------------------------------
-- Set inlet magnet valve
-- The inlet valve has a 50ms delay between request and operation
-- Increment the operations counter. The top bit will be able to be read via the SPI interface
--------------------------------------------------------------------------------------------------
	inlet_mv : process (reset_ni, clock20mhz_i) is
	begin
		if (reset_ni = '0') then
			counter_50ms_s 						<= 0;
			inlet_mv_s 								<= '0';
			inlet_mv_o 								<= '0';
			inlet_mv_counter_flag_s 			<= '0';
			inlet_mv_operations_counter_msb_o <= '0';
			inlet_mv_operations_counter_s 	<= (others => '0');
		elsif rising_Edge (clock20mhz_i) then

			-- map signal to output
			inlet_mv_o <= inlet_mv_s;

			-- inlet count msb goes to the mux
			inlet_mv_operations_counter_msb_o <= inlet_mv_operations_counter_s(10);
			
			-- inlet value requested to operate
			if inlet_mv_request_s = '1' then																		

				-- 50ms counter expired, so turn on valve
				if counter_50ms_s = counter_limit_50ms_c then												
					inlet_mv_s <= '1';
				
				-- 50ms count in progress so keep valve off
				elsif pulse_1ms_i = '1' then
					counter_50ms_s <= counter_50ms_s + 1;													
					inlet_mv_s <= '0';
				end if;

			else
				
				-- no requested, turn off vent valve and reset 50ms count
				counter_50ms_s <= 0;															
				inlet_mv_s <= '0';
				
			end if;
			
			-- create single clock pulse------------------------------------------
			inlet_mv_counter_flag_s <= inlet_mv_s;																

			if inlet_mv_counter_flag_s = '0' and inlet_mv_s = '1' then
				
				-- increment the operations counter
				inlet_mv_operations_counter_s <= inlet_mv_operations_counter_s + 1;					
			----------------------------------------------------------------------	
			
			end if;
		end if;
			
	end process;

-------------------------------------------------------------------------------------------------------
-- The vent valve has a 450ms delay between request and operation
-- Increment the operations counter. The top bit will be able to be read via the SPI interface
--------------------------------------------------------------------------------------------------------
	vent_mv : process (reset_ni, clock20mhz_i) is
	begin
		if (reset_ni = '0') then

			counter_450ms_s 			<= 0;
			vent_mv_s 					<= '0';
			vent_mv_o 					<= '0';
			vent_mv_counter_flag_s 	<= '0';
			vent_mv_operations_counter_msb_o <= '0';
			vent_mv_operations_counter_s <= ( others => '0');
			
		elsif rising_Edge (clock20mhz_i) then
		
			-- map signal to output
			vent_mv_o <= vent_mv_s;

			-- vent count msb goes to the mux
			vent_mv_operations_counter_msb_o <= vent_mv_operations_counter_s(10);
			

			-- vent value requested to operate
			if vent_mv_request_s = '1' then																		

				-- 450ms counter expired, so turn on valve
				if counter_450ms_s = counter_limit_450ms_c then												
					vent_mv_s <= '1';
					
				-- 450ms count in progress so keep valve off
				elsif pulse_1ms_i = '1' then
					counter_450ms_s <= counter_450ms_s + 1;													
					vent_mv_s <= '0';
				end if;

			-- no requested, turn off vent valve and reset 450ms count
			else
				counter_450ms_s <= 0;															
				vent_mv_s <= '0';
			end if;
			
			-- create single clock pulse------------------------------------------
			vent_mv_counter_flag_s <= vent_mv_s;																
			
			if vent_mv_counter_flag_s = '0' and vent_mv_s = '1' then
				-- increment the operations counter
				vent_mv_operations_counter_s <= vent_mv_operations_counter_s + 1;						
			end if;			
			----------------------------------------------------------------------	
			
			
		end if;
			
	end process;
			
-----------------------------------------------------------------------------------------




    
end architecture imp_valve_control;

